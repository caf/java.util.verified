/*
 * COPYRIGHT NOTICE FOR JAVA CODE:
 *
 * Copyright 1997-2007 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */
 
 
/*
 * COPYRIGHT NOTICE FOR JML ANNOTATIONS:
 *
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * The JML annotations are free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 */

 
package java_v.lang_v;


public final class Integer extends Number implements Comparable {

    public static final int MAX_VALUE = 2147483647;

    private /*@ spec_public @*/ final int intValue;

    /*@ public normal_behavior
      @   assignable this.intValue;
      @   ensures this.intValue == new_int_value;
      @*/
    //@ pure
    public Integer(int new_int_value) {
        this.intValue = new_int_value;
    }

    /*@ also
      @   public normal_behavior
      @     ensures \result == (byte) this.intValue;
      @*/
    //@ pure
    public byte byteValue() {
        return (byte) this.intValue;
    }

    /*@ also
      @   public normal_behavior
      @     ensures \result == (short) this.intValue;
      @*/
    public short shortValue() {
        return (short) this.intValue;
    }

    /*@ also
      @   public normal_behavior
      @     ensures \result == this.intValue;
      @*/
    public /*@ pure @*/ int intValue() {
        return this.intValue;
    }

    /*@ also
      @   public normal_behavior
      @     ensures \result == (long) this.intValue;
      @*/
    public long longValue() {
        return (long) this.intValue;
    }

    /*@ also
      @   public normal_behavior
      @      ensures \result == (float) this.intValue;
      @*/
    public float floatValue() {
        return (float) this.intValue;
    }

    /*@ also
      @   public normal_behavior
      @     ensures \result == (double) this.intValue;
      @*/
    public double doubleValue() {
        return (double) this.intValue;
    }

    /*@ also
      @   public normal_behavior
      @     ensures \result == this.intValue;
      @*/
    public int hashCode() {
        return this.intValue;
    }

    /*@ also
      @   public normal_behavior
      @     requires obj != null && (obj instanceof java_v.lang_v.Integer);
      @     ensures \result <==> this.intValue() == ((java_v.lang_v.Integer) obj).intValue();
      @   also
      @   public normal_behavior
      @     requires obj == null || !(obj instanceof java_v.lang_v.Integer);
      @     ensures !\result;
      @*/
    public boolean equals(/*@nullable*/Object obj) {
        if (obj == null || !(obj instanceof java_v.lang_v.Integer))
            return false;
        return this.intValue() == ((java_v.lang_v.Integer) obj).intValue();
    }

    /*@ public normal_behavior
      @ requires anotherInteger != null;
      @ {|
      @   requires this.intValue == anotherInteger.intValue();
      @   ensures \result == 0;
      @ also 
      @   requires this.intValue < anotherInteger.intValue();
      @   ensures \result == -1;
      @ also
      @   requires this.intValue > anotherInteger.intValue();
      @   ensures \result == 1;
      @ |}
      @ also public exceptional_behavior
      @   requires anotherInteger == null;
      @   signals_only NullPointerException;
      @*/
    public int compareTo(java_v.lang_v.Integer anotherInteger) {
        if (anotherInteger == null)
            throw new NullPointerException();
        if (this.intValue() == anotherInteger.intValue())
            return 0;
        if (this.intValue() < anotherInteger.intValue())
            return -1;
        if (this.intValue() > anotherInteger.intValue())
            return 1;
        // Unreacheable, but the compiler demands it.
        return 0;
    }

    /*@ also
      @   public normal_behavior
      @     requires o != null && (o instanceof java_v.lang_v.Integer);
      @     ensures \result == compareTo((java_v.lang_v.Integer) o);
      @ also
      @   public exceptional_behavior
      @     requires o != null && !(o instanceof java_v.lang_v.Integer);
      @     signals_only ClassCastException;
      @ also public exceptional_behavior
      @   requires o == null;
      @   signals_only NullPointerException;
      @*/
    public int compareTo(Object o) {
        if (o == null)
            throw new NullPointerException();
        if (!(o instanceof java_v.lang_v.Integer))
            throw new ClassCastException();
        return this.compareTo((java_v.lang_v.Integer) o);
    }

    /**
     * The only purpose of this method is allowing 
     * Chicory to access the private fields of this class.
     * 
     */
    private void unreachable_method() {
    }

    /*@ 
      @   public normal_behavior
      @     ensures \result == this.intValue;
      @*/
    public /*@ pure @*/ int hashCode0() {
        return this.intValue;
    }

}
