/*
 * COPYRIGHT NOTICE FOR JAVA CODE:
 *
 * Copyright 1997-2007 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */
 
 
/*
 * COPYRIGHT NOTICE FOR JML ANNOTATIONS:
 *
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * The JML annotations are free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 */


 
package java_v.lang_v;


public abstract class System {

   /*@ normal_behavior
     @ requires src != null & dest != null;
     @ requires length >= 0;
     @ requires 0 <= srcPos && srcPos + length <= src.length;
     @ requires 0 <= destPos && destPos + length <= dest.length;
     @ assignable dest[*];
     @ ensures (\forall int i; 0 <= i && i < destPos ==> dest[i] == \old(dest[i]));
     @ ensures (\forall int i; 0 <= i && i < length ==> dest[destPos + i] == \old(src[srcPos + i]));
     @ ensures (\forall int i; destPos + length <= i && i < dest.length; dest[i] == \old(dest[i]));
     @ ensures src != dest ==> (\forall int i; 0 <= i && i < src.length ==> src[i] == \old(src[i]));
     @
     @ also exceptional_behavior
     @ requires src == null || dest == null;
     @ signals (NullPointerException ex) src == null || dest == null;
     @
     @ also exceptional_behavior
     @ requires length < 0 || 
     @          (srcPos < 0 || srcPos + length > src.length) || 
     @          (destPos < 0 || destPos + length > dest.length);
     @ signals (java_v.lang_v.IndexOutOfBoundsException ex)
     @          length < 0 || 
     @          (srcPos < 0 || srcPos + length > src.length) || 
     @          (destPos < 0 || destPos + length > dest.length);
     @*/
   public static void arraycopyInteger(java_v.lang_v.Integer[] src, int srcPos, java_v.lang_v.Integer[] dest,
         int destPos, int length) throws NullPointerException, java_v.lang_v.IndexOutOfBoundsException {

      if (src == null)
         throw new NullPointerException();

      if (dest == null)
         throw new NullPointerException();

      if (length < 0)
         throw new java_v.lang_v.IndexOutOfBoundsException();

      if (srcPos < 0 || srcPos + length > src.length)
         throw new java_v.lang_v.IndexOutOfBoundsException();

      if (destPos < 0 || destPos + length > dest.length)
         throw new java_v.lang_v.IndexOutOfBoundsException();

      if (src == dest) {
         // Use temporary array
         java_v.lang_v.Integer[] temp_array = new java_v.lang_v.Integer[length];
         //@ loop_invariant 0 <= i && i <= length;
         //@ loop_invariant temp_array != src;
         //@ loop_invariant (\forall int j; 0 <= j && j < i ==> temp_array[j] == src[srcPos + j]);
         //@ loop_invariant (\forall int j; 0 <= j && j < dest.length ==> dest[j] == \old(dest[j]));
         //@ loop_invariant (\forall int j; 0 <= j && j < src.length ==> src[j] == \old(src[j]));
         for (int i = 0; i < length; i++) {
            temp_array[i] = src[srcPos + i];
         }
         /*@ loop_invariant 0 <= i && i <= length;
           @ loop_invariant (\forall int j; 0 <= j && j < i ==> dest[destPos + j] == temp_array[j]);
           @ loop_invariant (\forall int j; 0 <= j && j < destPos ==> dest[j] == \old(dest[j]));
           @ loop_invariant (\forall int j; 0 <= j && j < temp_array.length 
           @                                             ==> temp_array[j] == \old(temp_array[j]));
           @ loop_invariant (\forall int j; destPos + length <= j && j < dest.length
           @                                             ==> dest[j] == \old(dest[j])); @*/
         for (int i = 0; i < length; i++) {
            dest[destPos + i] = temp_array[i];
         }
      } else {
          /*@ loop_invariant 0 <= i && i <= length;
            @ loop_invariant (\forall int j; 0 <= j && j < src.length ==> src[j] == \old(src[j]));
            @ loop_invariant (\forall int j; 0 <= j && j < i ==> dest[destPos + j] == src[srcPos + j]);
            @ loop_invariant (\forall int j; 0 <= j && j < destPos ==> dest[j] == \old(dest[j]));
            @ loop_invariant (\forall int j; destPos + length <= j && j < dest.length 
            @                                            ==> dest[j] == \old(dest[j])); @*/
         for (int i = 0; i < length; i++) {
            dest[destPos + i] = src[srcPos + i];
         }
      }
   }

   /*@ normal_behavior
     @ requires src != null & dest != null;
     @ requires length >= 0;
     @ requires srcPos >= 0 && srcPos + length <= src.length;
     @ requires destPos >= 0 && destPos + length <= dest.length;
     @ assignable src[*], dest[*];
     @ ensures src != dest ==> (\forall int i; 0 <= i && i < src.length ==> src[i] == \old(src[i]));
     @ ensures (\forall int i; 0 <= i && i < length ==> dest[destPos + i] == \old(src[srcPos + i]));
     @ ensures (\forall int i; 0 <= i && i < destPos ==> dest[i] == \old(dest[i]));
     @ ensures (\forall int i; destPos + length <= i && i < dest.length ==> dest[i] == \old(dest[i]));
     @
     @ also exceptional_behavior
     @ requires src == null || dest == null;
     @ signals (NullPointerException ex) src == null || dest == null;
     @
     @ also exceptional_behavior
     @ requires length < 0 || 
     @          (srcPos < 0 || srcPos + length > src.length) || 
     @          (destPos < 0 || destPos + length > dest.length);
     @ signals (IndexOutOfBoundsException ex)
     @          length < 0 || 
     @          (srcPos < 0 || srcPos + length > src.length) || 
     @          (destPos < 0 || destPos + length > dest.length);
     @*/
   public static void arraycopy(int[] src, int srcPos, int[] dest,
         int destPos, int length) throws NullPointerException, java_v.lang_v.IndexOutOfBoundsException {
      if (src == null)
         throw new NullPointerException();

      if (dest == null)
         throw new NullPointerException();

      if (length < 0)
         throw new java_v.lang_v.IndexOutOfBoundsException();

      if (srcPos < 0 || srcPos + length > src.length)
         throw new java_v.lang_v.IndexOutOfBoundsException();

      if (destPos < 0 || destPos + length > dest.length)
         throw new java_v.lang_v.IndexOutOfBoundsException();

      if (src == dest) {
         // Use temporary array
         int[] temp_array = new int[length];
         //@ loop_invariant 0 <= i && i <= length;
         //@ loop_invariant temp_array != src;
         //@ loop_invariant (\forall int j; 0 <= j && j < i ==> temp_array[j] == src[srcPos + j]);
         //@ loop_invariant (\forall int j; 0 <= j && j < dest.length ==> dest[j] == \old(dest[j]));
         //@ loop_invariant (\forall int j; 0 <= j && j < src.length ==> src[j] == \old(src[j]));
         for (int i = 0; i < length; i++) {
            temp_array[i] = src[srcPos + i];
         }
         /*@ loop_invariant 0 <= i && i <= length;
           @ loop_invariant (\forall int j; 0 <= j && j < i ==> dest[destPos + j] == temp_array[j]);
           @ loop_invariant (\forall int j; 0 <= j && j < destPos ==> dest[j] == \old(dest[j]));
           @ loop_invariant (\forall int j; 0 <= j && j < temp_array.length 
           @                                          ==> temp_array[j]==\old(temp_array[j]));
           @ loop_invariant (\forall int j; destPos + length <= j && j < dest.length 
           @                                          ==> dest[j] == \old(dest[j])); @*/
         for (int i = 0; i < length; i++) {
            dest[destPos + i] = temp_array[i];
         }
      } else {
          /*@ loop_invariant 0 <= i && i <= length;
            @ loop_invariant (\forall int j; 0 <= j && j < src.length ==> src[j] == \old(src[j]));
            @ loop_invariant (\forall int j; 0 <= j && j < i ==> dest[destPos + j] == src[srcPos + j]);
            @ loop_invariant (\forall int j; 0 <= j && j < destPos ==> dest[j] == \old(dest[j]));
            @ loop_invariant (\forall int j; destPos + length <= j && j < dest.length 
            @                                         ==> dest[j] == \old(dest[j])); @*/
         for (int i = 0; i < length; i++) {
            dest[destPos + i] = src[srcPos + i];
         }
      }

   }

}
