/*
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * This file is part of the JML-annotated java.util package written as 
 * part of the <a href="www.st.cs.uni-saarland.de/dynamate/">DynaMate</a> 
 * project.
 *
 * DynaMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DynaMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this package.  If not, see <http://www.gnu.org/licenses/>.
 *
 */



package java_v.theories;


public class QTArrays {

    /*@
      @ requires a != null;
      @
      @ ensures \result <==> (fromIndex <= toIndex) &&
      @                      ((!neg  && 0 <= fromIndex + off  &&  toIndex + off <= a.length) ||
      @                       ( neg  && 0 <= off - toIndex  &&  off - fromIndex <= a.length));
      @*/
    public static /*@ pure @*/ boolean Qwithin(int fromIndex, int toIndex, int[] a, int off, boolean neg)
    {
        if (a == null)
            throw new RuntimeException();

        return ((fromIndex <= toIndex) &&
                ((!neg  && 0 <= fromIndex + off  &&  toIndex + off <= a.length) ||
                 ( neg  && 0 <= off - toIndex  &&  off - fromIndex <= a.length)));
    }

    /*@
      @ requires a != null;
      @
      @ ensures \result <==> (fromIndex <= toIndex) &&
      @                      ((!neg  && 0 <= fromIndex + off  &&  toIndex + off <= a.length) ||
      @                       ( neg  && 0 <= off - toIndex  &&  off - fromIndex <= a.length));
      @*/
    public static /*@ pure @*/ boolean Qwithin(int fromIndex, int toIndex, java_v.lang_v.Integer[] a, int off, boolean neg)
    {
        if (a == null)
            throw new RuntimeException();

        return ((fromIndex <= toIndex) &&
                ((!neg  && 0 <= fromIndex + off  &&  toIndex + off <= a.length) ||
                 ( neg  && 0 <= off - toIndex  &&  off - fromIndex <= a.length)));
    }

    /*@
      @ requires a != null && b != null;
      @ requires Qwithin(fromIndex, toIndex, a, off_a, neg_a);
      @ requires Qwithin(fromIndex, toIndex, b, off_b, neg_b);
      @
      @ ensures \result <==>
      @  (\forall int j; fromIndex <= j && j < toIndex ==> 
      @                a[off_a + (neg_a ? -j : j)] == b[off_b + (neg_b ? -j : j)]);
      @*/
    public static /*@ pure @*/ boolean QEq(int fromIndex, int toIndex, 
                                           int[] a, int off_a, boolean neg_a, 
                                           int[] b, int off_b, boolean neg_b)
    {
        if (a == null || b == null)
            throw new RuntimeException();
        if (!Qwithin(fromIndex, toIndex, a, off_a, neg_a))
            throw new RuntimeException();
        if (!Qwithin(fromIndex, toIndex, b, off_b, neg_b))
            throw new RuntimeException();

        if (toIndex <= fromIndex) {
            return true;
        } else {
            return a[off_a + (neg_a ? -fromIndex : fromIndex)] == 
                b[off_b + (neg_a ? -fromIndex : fromIndex)] && 
                QEq(fromIndex + 1, toIndex, a, off_a, neg_a, b, off_b, neg_b);
        }
    }

    /*@
      @ requires a != null;
      @ requires Qwithin(fromIndex, toIndex, a, off_a, neg_a);
      @
      @ ensures \result <==>
      @  (\forall int j; fromIndex <= j && j < toIndex ==> 
      @                a[off_a + (neg_a ? -j : j)] == K);
      @*/
    public static /*@ pure @*/ boolean QEqK(int fromIndex, int toIndex, 
                                            int[] a, int off_a, boolean neg_a, 
                                            int K)
    {
        if (a == null)
            throw new RuntimeException();
        if (!Qwithin(fromIndex, toIndex, a, off_a, neg_a))
            throw new RuntimeException();

        if (toIndex <= fromIndex) {
            return true;
        } else {
            return a[off_a + (neg_a ? -fromIndex : fromIndex)] == 
                K && 
                QEqK(fromIndex + 1, toIndex, a, off_a, neg_a, K);
        }
    }

    /*@
      @ requires a != null && b != null;
      @ requires Qwithin(fromIndex, toIndex, a, off_a, neg_a);
      @ requires Qwithin(fromIndex, toIndex, b, off_b, neg_b);
      @
      @ ensures \result <==>
      @  (\forall int j; fromIndex <= j && j < toIndex ==> 
      @        (a[off_a + (neg_a ? -j : j)] == null && b[off_b + (neg_b ? -j : j)] == null) ||
      @        (a[off_a + (neg_a ? -j : j)] != null && b[off_b + (neg_b ? -j : j)] != null && 
      @            a[off_a + (neg_a ? -j : j)].intValue() == b[off_b + (neg_b ? -j : j)].intValue()));
      @*/
    public static /*@ pure @*/ boolean QEq(int fromIndex, int toIndex,
                                           java_v.lang_v.Integer[] a, int off_a, boolean neg_a, 
                                           java_v.lang_v.Integer[] b, int off_b, boolean neg_b)
    {
        if (a == null || b == null)
            throw new RuntimeException();
        if (!Qwithin(fromIndex, toIndex, a, off_a, neg_a))
            throw new RuntimeException();
        if (!Qwithin(fromIndex, toIndex, b, off_b, neg_b))
            throw new RuntimeException();

        if (toIndex <= fromIndex) {
            return true;
        } else {
            java_v.lang_v.Integer f_a = a[off_a + (neg_a ? -fromIndex : fromIndex)], 
                f_b = b[off_b + (neg_b ? -fromIndex : fromIndex)];
            return ((f_a == null && f_b == null) || 
                    (f_a != null && f_b != null && f_a.intValue() == f_b.intValue())) &&
                QEq(fromIndex + 1, toIndex, a, off_a, neg_a, b, off_b, neg_b);
        }
    }

    /*@
      @ requires a != null;
      @ requires Qwithin(fromIndex, toIndex, a, off_a, neg_a);
      @
      @ ensures \result <==>
      @  (\forall int j; fromIndex <= j && j < toIndex ==> 
      @        (a[off_a + (neg_a ? -j : j)] == null && K == null) ||
      @        (a[off_a + (neg_a ? -j : j)] != null && K != null && 
      @            a[off_a + (neg_a ? -j : j)].intValue() == K.intValue()));
      @*/
    public static /*@ pure @*/ boolean QEqK(int fromIndex, int toIndex,
                                            java_v.lang_v.Integer[] a, int off_a, boolean neg_a, 
                                            java_v.lang_v.Integer K)
    {
        if (a == null)
            throw new RuntimeException();
        if (!Qwithin(fromIndex, toIndex, a, off_a, neg_a))
            throw new RuntimeException();

        if (toIndex <= fromIndex) {
            return true;
        } else {
            java_v.lang_v.Integer f_a = a[off_a + (neg_a ? -fromIndex : fromIndex)];
            return ((f_a == null && K == null) || 
                    (f_a != null && K != null && f_a.intValue() == K.intValue())) &&
                QEqK(fromIndex + 1, toIndex, a, off_a, neg_a, K);
        }
    }

}
