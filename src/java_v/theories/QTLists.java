/*
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * This file is part of the JML-annotated java.util package written as 
 * part of the <a href="www.st.cs.uni-saarland.de/dynamate/">DynaMate</a> 
 * project.
 *
 * DynaMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DynaMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this package.  If not, see <http://www.gnu.org/licenses/>.
 *
 */



package java_v.theories;


public class QTLists {

    public static /*@ pure @*/ boolean QEq(int fromIndex, int toIndex, 
                                           String call_a, Object a, int off_a, boolean neg_a, 
                                           String call_b, Object b, int off_b, boolean neg_b)
    {
        if (toIndex <= fromIndex) {
            return true;
        } else {
            java.lang.reflect.Method method_a=null, method_b=null;
            try {
                method_a = a.getClass().getMethod(call_a, int.class);
                method_b = b.getClass().getMethod(call_b, int.class);
            } catch (SecurityException e) {
                System.out.println("SecurityException when trying to call: " + call_a + " or " + call_b);
            } catch (NoSuchMethodException e) {
                System.out.println("NoSuchMethodException when trying to call: " + call_a + " or " + call_b);
            }
            java_v.lang_v.Integer va=null, vb=null;
            try {
                va = (java_v.lang_v.Integer) 
                    method_a.invoke(a, off_a + (neg_a ? -fromIndex : fromIndex));
                vb = (java_v.lang_v.Integer) 
                    method_b.invoke(b, off_b + (neg_b ? -fromIndex : fromIndex));
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            } catch (java.lang.reflect.InvocationTargetException e) {
            }
            return ((va == null && vb == null) || 
                    (va != null && vb != null && va.intValue() == vb.intValue())) && 
                QEq(fromIndex + 1, toIndex, call_a, a, off_a, neg_a, call_b, b, off_b, neg_b);
        }
    }


    public static /*@ pure @*/ boolean QEq(int fromIndex, int toIndex, 
                                           String call_a, Object a, int off_a, boolean neg_a, 
                                           java_v.lang_v.Integer[] b, int off_b, boolean neg_b)
    {
        if (toIndex <= fromIndex) {
            return true;
        } else {
            java.lang.reflect.Method method_a=null;
            try {
                method_a = a.getClass().getMethod(call_a, int.class);
            } catch (SecurityException e) {
                System.out.println("SecurityException when trying to call: " + call_a);
            } catch (NoSuchMethodException e) {
                System.out.println("NoSuchMethodException when trying to call: " + call_a);
            }
            java_v.lang_v.Integer va=null, vb=null;
            try {
                va = (java_v.lang_v.Integer) 
                    method_a.invoke(a, off_a + (neg_a ? -fromIndex : fromIndex));
                vb = b[off_b + (neg_b ? -fromIndex : fromIndex)];
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            } catch (java.lang.reflect.InvocationTargetException e) {
            }
            return ((va == null && vb == null) || 
                    (va != null && vb != null && va.intValue() == vb.intValue())) && 
                QEq(fromIndex + 1, toIndex, call_a, a, off_a, neg_a, b, off_b, neg_b);
        }
    }

}
