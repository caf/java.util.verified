/*
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * This file is part of the JML-annotated java.util package written as 
 * part of the <a href="www.st.cs.uni-saarland.de/dynamate/">DynaMate</a> 
 * project.
 *
 * DynaMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DynaMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this package.  If not, see <http://www.gnu.org/licenses/>.
 *
 */



package java_v.theories;

import java_v.util_v.ArrayList;
import java_v.theories.TArrays;
import java_v.lang_v.Integer;


public class TLists {

    // Is `[fromIndex..toIndex)' a valid interval within `[0..a.length)'?
    /*@ 
      @ requires a != null;
      @
      @ ensures \result <==> 0 <= fromIndex && fromIndex <= toIndex && toIndex <= a.length; 
      @*/
    public static /*@ pure @*/ boolean within(java_v.lang_v.Integer[] a, int fromIndex, int toIndex)
    {
        if (a == null)
            throw new RuntimeException();

        return (0 <= fromIndex && fromIndex <= toIndex && toIndex <= a.length);
    }

    // Does `a' contain an element `key' within [fromIndex..toIndex)?
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ 
      @ ensures \result <==> (\exists int i; fromIndex <= i && i < toIndex && key == a[i]); 
      @*/
    public static /*@ pure @*/ boolean in(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                          java_v.lang_v.Integer key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return false;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first == key) || in(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) == val?
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ 
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] == val); 
      @*/
    public static /*@ pure @*/ boolean eq(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                          java_v.lang_v.Integer val)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first == val) && eq(a, fromIndex + 1, toIndex, val);
        }
    }

    // Does a[fromIndex..toIndex) != val?
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] != val); 
      @*/
    public static /*@ pure @*/ boolean neq(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                           java_v.lang_v.Integer val)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first != val) && neq(a, fromIndex + 1, toIndex, val);
        }
    }

    // Does the triple (els, head, tail) define a valid deq?
    /*@ 
      @ ensures \result <==> els != null && ( 0 <= head && head < els.length &&
      @                                       0 <= tail && tail < els.length &&
      @                                       els[tail] == null );
      @*/
    public static /*@ pure @*/ boolean deqValid(java_v.lang_v.Integer[] els, int head, int tail)
    {
        return ( els != null && 0 <= head && head < els.length &&
                                0 <= tail && tail < els.length &&
                                els[tail] == null );
    }

    // Is `ic' a valid index within the deq defined by (els, head, tail)?
    /*@ 
      @ requires deqValid(els, head, tail); 
      @
      @ ensures \result <==> 
      @          (head <= tail ==> head <= ic && ic <= tail) &&
      @          (head > tail ==> head <= ic && ic < els.length || 0 <= ic && ic <= tail); 
      @*/
    public static /*@ pure @*/ boolean deqWithin(java_v.lang_v.Integer[] els, int head, int tail, int ic)
    {
        if (!deqValid(els, head, tail))
            throw new RuntimeException();

        if (head <= tail) {
            return head <= ic && ic <= tail;
        } else {
            return head <= ic && ic < els.length || 0 <= ic && ic <= tail;
        }
    }

    // Does the deq defined by (els, head, tail) has no element with value `val' within `[head..ic)'?
    // The interval `[head..ic)' wraps over if tail < head.
    /*@ 
      @ requires deqValid(els, head, tail);
      @ requires deqWithin(els, head, tail, ic);
      @
      @ ensures \result <==> ( 
      @      (head <= tail ==> TLists.neq(els, head, ic, val)) &&
      @      (head > tail ==> 
      @            (head <= ic && ic < els.length ==> neq(els, head, ic, val)) &&
      @            (0 <= ic && ic <= tail ==> neq(els, head, els.length, val) && neq(els, 0, ic, val))) ); 
      @*/
    public static /*@ pure @*/ boolean deqHasNot(java_v.lang_v.Integer[] els, int head, int tail, int ic, 
                                                 java_v.lang_v.Integer val)
    {
        if (!deqValid(els, head, tail))
            throw new RuntimeException();
        if (!deqWithin(els, head, tail, ic))
            throw new RuntimeException();

        if (head <= tail) {
            return neq(els, head, ic, val);
        } else {
            if (head <= ic && ic < els.length)
                return neq(els, head, ic, val);
            else
                return neq(els, head, els.length, val) && neq(els, 0, ic, val);
        }
    }

    // Does the deq defined by (els, head, tail) has an element with value `val' within `[head..ic)'?
    // The interval `[head..ic)' wraps over if tail < head.
    /*@ 
      @ requires deqValid(els, head, tail);
      @ requires deqWithin(els, head, tail, ic);
      @
      @ ensures \result <==> ( 
      @   (head <= tail ==> !TLists.neq(els, head, ic, val)) &&
      @   (head > tail ==> 
      @      ( (head <= ic && ic < els.length && !neq(els, head, ic, val)) ||
      @        (0 <= ic && ic <= tail && (!neq(els, head, els.length, val) || !neq(els, 0, ic, val))) ))); 
      @*/
    public static /*@ pure @*/ boolean deqHas(java_v.lang_v.Integer[] els, int head, int tail, int ic, 
                                              java_v.lang_v.Integer val)
    {
        if (!deqValid(els, head, tail))
            throw new RuntimeException();
        if (!deqWithin(els, head, tail, ic))
            throw new RuntimeException();

        return !deqHasNot(els, head, tail, ic, val);
    }

    // Does the deq defined by (els, head, tail) has elements all equal to `val' within `[head..ic)'?
    // The interval `[head..ic)' wraps over if tail < head.
    /*@ 
      @ requires deqValid(els, head, tail);
      @ requires deqWithin(els, head, tail, ic);
      @
      @ ensures \result <==> ( 
      @      (head <= tail ==> TLists.eq(els, head, ic, val)) &&
      @      (head > tail ==> 
      @            (head <= ic && ic < els.length ==> eq(els, head, ic, val)) &&
      @            (0 <= ic && ic <= tail ==> eq(els, head, els.length, val) && eq(els, 0, ic, val))) ); 
      @*/
    public static /*@ pure @*/ boolean deqEq(java_v.lang_v.Integer[] els, int head, int tail, int ic, 
                                             java_v.lang_v.Integer val)
    {
        if (!deqValid(els, head, tail))
            throw new RuntimeException();
        if (!deqWithin(els, head, tail, ic))
            throw new RuntimeException();

        if (head <= tail) {
            return eq(els, head, ic, val);
        } else {
            if (head <= ic && ic < els.length)
                return eq(els, head, ic, val);
            else
                return eq(els, head, els.length, val) && eq(els, 0, ic, val);
        }
    }

    // Is `[fromIndex..toIndex)' a valid interval within `al'?
    /*@ 
      @ requires al != null;
      @
      @ ensures \result <==> 0 <= fromIndex && fromIndex <= toIndex && toIndex <= al.size; 
      @*/
    public static /*@ pure @*/ boolean within(ArrayList al, int fromIndex, int toIndex)
    {
        if (al == null)
            throw new RuntimeException();

        return 0 <= fromIndex && fromIndex <= toIndex && toIndex <= al.size();
    }

    // Does al[fromIndex..toIndex) != val?
    /*@ 
      @ requires al != null;
      @ requires within(al, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> al.elementData[i] != val); 
      @*/
    public static /*@ pure @*/ boolean neq(ArrayList al, int fromIndex, int toIndex, java_v.lang_v.Integer val)
    {
        if (al == null)
            throw new RuntimeException();
        if (!within(al, fromIndex, toIndex))
            throw new RuntimeException();

        return neq(al.getElementData(), fromIndex, toIndex, val);
    }

    // Is the list `al' sorted in ascending order over [`fromIndex'..`toIndex')?
    /*@ 
      @ requires al != null;
      @ requires within(al, fromIndex, toIndex);
      @ requires neq(al, fromIndex, toIndex, null);
      @
      @ ensures \result <==> (\forall int i, j; fromIndex <= i && i < j && j < toIndex 
      @                             ==> al.elementData[i].intValue() <= al.elementData[j].intValue()); 
      @ ensures \result <==> TArrays.sorted(al.getElementData(), fromIndex, toIndex); 
      @*/
    public static /*@ pure @*/ boolean sorted(ArrayList al, int fromIndex, int toIndex)
    {
        if (al == null)
            throw new RuntimeException();
        if (!within(al, fromIndex, toIndex))
            throw new RuntimeException();
        if (!neq(al, fromIndex, toIndex, null))
            throw new RuntimeException();

        return TArrays.sorted(al.getElementData(), fromIndex, toIndex);
    }

}
