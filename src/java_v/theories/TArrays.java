/*
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * This file is part of the JML-annotated java.util package written as 
 * part of the <a href="www.st.cs.uni-saarland.de/dynamate/">DynaMate</a> 
 * project.
 *
 * DynaMate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DynaMate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this package.  If not, see <http://www.gnu.org/licenses/>.
 *
 */



package java_v.theories;


public class TArrays {

    // Is `a' empty?
    /**
     * Returns <code>true</code> iff <code>a</code> is empty.
     * 
     * @param a   an array
     * @return    <code>true</code> if <code>a</code> is empty, otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require  <code>a != null</code>
     * @dm.ensure   <code>\result <==> (a.length == 0)</code>
     */
    /*@ 
      @ requires a != null;
      @
      @ ensures \result <==> (a.length == 0); 
      @*/
    public static /*@ pure @*/ boolean empty(int[] a)
    {
        if (a == null)
            throw new RuntimeException();

        return a.length == 0;
    }

    // Is x a valid position in `a'?
    /**
     * Returns <code>true</code> iff <code>x</code> is a valid index in <code>a</code>.
     * 
     * @param a   an array
     * @param x   an <code>int</code> value
     * @return    <code>true</code> if <code>x</code> is a valid index in <code>a</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require  <code>a != null</code>
     * @dm.ensure   <code>\result <==> 0 <= x && x < a.length</code>
     */
    /*@ 
      @ requires a != null;
      @
      @ ensures \result <==> 0 <= x && x < a.length; 
      @*/
    public static /*@ pure @*/ boolean in_bound(int[] a, int x)
    {
        if (a == null)
            throw new RuntimeException();

        return (0 <= x && x < a.length);
    }
    
    // Is `[fromIndex..toIndex)' a valid interval within `[0..a.length)'?
    /**
     * Returns <code>true</code> iff <code>[fromIndex..toIndex)</code> 
       is a subinterval of <code>[0..a.length)</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @return    <code>true</code> if <code>x</code> is a valid index in <code>a</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require  <code>a != null</code>
     * @dm.ensure   <code>\result <==> 0 <= fromIndex && fromIndex <= toIndex && toIndex <= a.length</code>
     */
    /*@ 
      @ requires a != null;
      @
      @ ensures \result <==> 0 <= fromIndex && fromIndex <= toIndex && toIndex <= a.length; 
      @*/
    public static /*@ pure @*/ boolean within(int[] a, int fromIndex, int toIndex)
    {
        if (a == null)
            throw new RuntimeException();

        return (0 <= fromIndex && fromIndex <= toIndex && toIndex <= a.length);
    }

    // Is `[fromIndex..toIndex)' a valid interval within `[0..a.length)'?
    /**
     * Returns <code>true</code> iff <code>[fromIndex..toIndex)</code> 
       is a subinterval of <code>[0..a.length)</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @return    <code>true</code> if <code>x</code> is a valid index in <code>a</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.ensure    <code>\result <==> 0 <= fromIndex && fromIndex <= toIndex && toIndex <= a.length</code>
     */
    /*@ 
      @ requires a != null;
      @
      @ ensures \result <==> 0 <= fromIndex && fromIndex <= toIndex && toIndex <= a.length; 
      @*/
    public static /*@ pure @*/ boolean within(java_v.lang_v.Integer[] a, int fromIndex, int toIndex)
    {
        if (a == null)
            throw new RuntimeException();

        return (0 <= fromIndex && fromIndex <= toIndex && toIndex <= a.length);
    }

    // Is `a' sorted within `[fromIndex..toIndex)'?
    /**
     * Returns <code>true</code> iff <code>a</code> is sorted over <code>[fromIndex..toIndex)</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @return    <code>true</code> if <code>a</code> is sorted over <code>[fromIndex..toIndex)</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    <code>\result <==> 
                     (\forall int i, j; fromIndex <= i && i < j && j < toIndex ==> a[i] <= a[j])</code>
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i, j; fromIndex <= i && i < j && j < toIndex ==> a[i] <= a[j]); 
      @*/
    public static /*@ pure @*/ boolean sorted(int[] a, int fromIndex, int toIndex)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (toIndex - fromIndex <= 1) {
            return true;
        } else {
            int first = a[fromIndex], second = a[fromIndex + 1];
            return first <= second && sorted(a, fromIndex + 1, toIndex);
        }
    }

    // Is `a' sorted within `[fromIndex..toIndex)'?
    /**
     * Returns <code>true</code> iff <code>a</code> is sorted over <code>[fromIndex..toIndex)</code>.
     * 
     * @param a   an array without <code>null</code> values over <code>[fromIndex..toIndex)</code>
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @return    <code>true</code> if <code>a</code> is sorted over <code>[fromIndex..toIndex)</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.require   <code>nonnull(a, fromIndex, toIndex)</code>
     * @dm.ensure    <code>\result <==> (\forall int i, j; 
                        fromIndex <= i && i < j && j < toIndex ==> a[i].intValue() <= a[j].intValue())</code>
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ requires nonnull(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i, j; 
      @                 fromIndex <= i && i < j && j < toIndex ==> a[i].intValue() <= a[j].intValue()); 
      @*/
    public static /*@ pure @*/ boolean sorted(java_v.lang_v.Integer[] a, int fromIndex, int toIndex)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (!nonnull(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (toIndex - fromIndex <= 1) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex], second = a[fromIndex + 1];
            return first.intValue() <= second.intValue() && sorted(a, fromIndex + 1, toIndex);
        }
    }

    // Does `a' contain an element `key' within [fromIndex..toIndex)?
    /**
     * Returns <code>true</code> iff <code>a</code> has 
       an element with value <code>key</code> over <code>[fromIndex..toIndex)</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>int</code> value
     * @return    <code>true</code> if <code>key</code> appears 
                  in <code>a</code> over <code>[fromIndex..toIndex)</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    <code>\result <==> (\exists int i; fromIndex <= i && i < toIndex && key == a[i])</code>
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\exists int i; fromIndex <= i && i < toIndex && key == a[i]); 
      @*/
    public static /*@ pure @*/ boolean in(int[] a, int fromIndex, int toIndex, int key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return false;
        } else {
            int first = a[fromIndex];
            return (first == key) || in(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) < key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value less than <code>key</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>int</code> value
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value less than <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] < key)
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ 
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] < key); 
      @*/
    public static /*@ pure @*/ boolean less(int[] a, int fromIndex, int toIndex, int key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            int first = a[fromIndex];
            return (first < key) && less(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) < key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value less than <code>key</code>.
     * 
     * @param a   an array without <code>null</code> values over <code>[fromIndex..toIndex)</code>
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>Integer</code> value, not <code>null</code>
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value less than <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.require   <code>nonnull(a, fromIndex, toIndex)</code>
     * @dm.require   <code>key != null</code>
     * @dm.ensure    \result <==> (\forall int i; 
                             fromIndex <= i && i < toIndex ==> a[i].intValue() < key.intValue())
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ requires nonnull(a, fromIndex, toIndex);
      @ requires key != null;
      @
      @ ensures \result <==> 
      @            (\forall int i; fromIndex <= i && i < toIndex ==> a[i].intValue() < key.intValue());
      @*/
    public static /*@ pure @*/ boolean less(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                              java_v.lang_v.Integer key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (!nonnull(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (key == null)
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first.intValue() < key.intValue()) && less(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) <= key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value less than or equal to <code>key</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>int</code> value
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value less than or equal to <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
    * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] <= key)
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] <= key); 
      @*/
    public static /*@ pure @*/ boolean lesseq(int[] a, int fromIndex, int toIndex, int key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            int first = a[fromIndex];
            return (first <= key) && lesseq(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) <= key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value less than or equal to <code>key</code>.
     * 
     * @param a   an array without <code>null</code> values over <code>[fromIndex..toIndex)</code>
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>Integer</code> value, not <code>null</code>
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value less than or equal to <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.require   <code>nonnull(a, fromIndex, toIndex)</code>
     * @dm.require   <code>key != null</code>
     * @dm.ensure    \result <==> 
                (\forall int i; fromIndex <= i && i < toIndex ==> a[i].intValue() <= key.intValue())
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ requires nonnull(a, fromIndex, toIndex);
      @ requires key != null;
      @
      @ ensures \result <==> 
      @            (\forall int i; fromIndex <= i && i < toIndex ==> a[i].intValue() <= key.intValue());
      @*/
    public static /*@ pure @*/ boolean lesseq(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                              java_v.lang_v.Integer key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (!nonnull(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (key == null)
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first.intValue() <= key.intValue()) && lesseq(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) > key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value greater than <code>key</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>int</code> value
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value greater than <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] > key)
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] > key); 
      @*/
    public static /*@ pure @*/ boolean grt(int[] a, int fromIndex, int toIndex, int key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            int first = a[fromIndex];
            return (first > key) && grt(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) > key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value greater than <code>key</code>.
     * 
     * @param a   an array without <code>null</code> values over <code>[fromIndex..toIndex)</code>
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>Integer</code> value, not <code>null</code>
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value greater than <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.require   <code>nonnull(a, fromIndex, toIndex)</code>
     * @dm.require   <code>key != null</code>
     * @dm.ensure    \result <==> (\forall int i; 
                         fromIndex <= i && i < toIndex ==> a[i].intValue() > key.intValue())
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ requires nonnull(a, fromIndex, toIndex);
      @ requires key != null;
      @
      @ ensures \result <==> (\forall int i; 
      @                    fromIndex <= i && i < toIndex ==> a[i].intValue() > key.intValue()); 
      @*/
    public static /*@ pure @*/ boolean grt(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                           java_v.lang_v.Integer key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (!nonnull(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (key == null)
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (key.intValue() < first.intValue()) && grt(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) >= key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value greater than or equal to <code>key</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>int</code> value
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value greater than or equal to <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] >= key)
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] >= key); 
      @*/
    public static  /*@ pure @*/ boolean grteq(int[] a, int fromIndex, int toIndex, int key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            int first = a[fromIndex];
            return (first >= key) && grteq(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) >= key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value greater than or equal to <code>key</code>.
     * 
     * @param a   an array without <code>null</code> values over <code>[fromIndex..toIndex)</code>
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>Integer</code> value, not <code>null</code>
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value greater than or equal to  <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.require   <code>nonnull(a, fromIndex, toIndex)</code>
     * @dm.require   <code>key != null</code>
     * @dm.ensure    \result <==> 
                   (\forall int i; fromIndex <= i && i < toIndex ==> a[i].intValue() >= key.intValue())
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ requires nonnull(a, fromIndex, toIndex);
      @ requires key != null;
      @
      @ ensures \result <==> (\forall int i; 
      @                      fromIndex <= i && i < toIndex ==> a[i].intValue() >= key.intValue()); 
      @*/
    public static  /*@ pure @*/ boolean grteq(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                              java_v.lang_v.Integer key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (!nonnull(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (key == null)
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first.intValue() >= key.intValue()) && grteq(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[aFrom..aTo) == b[bFrom..bTo)?
    /**
     * Returns <code>true</code> iff the sequences of values 
       <code>a[aFrom..aTo)</code> and <code>b[bFrom..bTo)</code> are identical.
     * 
     * @param a   an array
     * @param aFrom   a lower <code>int</code> index in <code>a</code>
     * @param aTo   an upper <code>int</code> index in <code>a</code>
     * @param b   an array
     * @param bFrom   a lower <code>int</code> index in <code>b</code>
     * @param bTo   an upper <code>int</code> index in <code>b</code>
     * @return    <code>true</code> if 
                  the sequence of elements of <code>a</code> over <code>[aFrom..aTo)</code> is the same as 
                  the sequence of elements of <code>b</code> over <code>[bFrom..bTo)</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null && b != null</code>
     * @dm.require   <code>within(a, aFrom, aTo) && within(b, bFrom, bTo)</code>
     * @dm.ensure    \result <==> aTo - aFrom == bTo - bFrom && 
                   (\forall int i; aFrom <= i && i < aTo ==> a[i] == b[bFrom + (i - aFrom)])
     */
    /*@ 
      @ requires a != null && b != null;
      @ requires within(a, aFrom, aTo) && within(b, bFrom, bTo);
      @
      @ ensures \result <==> aTo - aFrom == bTo - bFrom && 
      @                      (\forall int i; aFrom <= i && i < aTo ==> a[i] == b[bFrom + (i - aFrom)]); 
      @*/
    public static /*@ pure @*/ boolean eq(int[] a, int aFrom, int aTo, int[] b, int bFrom, int bTo)
    {
        if (a == null || b == null)
            throw new RuntimeException();
        if (!within(a, aFrom, aTo) || !within(b, bFrom, bTo))
            throw new RuntimeException();

        if (aTo - aFrom != bTo - bFrom) {
            return false;
        } else { if (aFrom >= aTo) {
                return true;
            } else {
                return (a[aFrom] == b[bFrom]) && eq(a, aFrom + 1, aTo, b, bFrom + 1, bTo);
            }
        }
    }

    // Does a[fromIndex..toIndex) == key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value equal to <code>key</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>int</code> value
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value equal to <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] == key)
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] == key); 
      @*/
    public static /*@ pure @*/ boolean eq(int[] a, int fromIndex, int toIndex, int key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            int first = a[fromIndex];
            return (first == key) && eq(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) == key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value equal to <code>key</code>.
     * 
     * @param a   an array without <code>null</code> values over <code>[fromIndex..toIndex)</code>
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>Integer</code> value, not <code>null</code>
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value equal to  <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.require   <code>nonnull(a, fromIndex, toIndex)</code>
     * @dm.require   <code>key != null</code>
     * @dm.ensure    \result <==> 
                   (\forall int i; fromIndex <= i && i < toIndex ==> a[i].intValue() == key.intValue())
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ requires nonnull(a, fromIndex, toIndex);
      @ requires key != null;
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex 
      @                                 ==> a[i].intValue() == key.intValue()); 
      @*/
    public static /*@ pure @*/ boolean eq(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                          java_v.lang_v.Integer key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (!nonnull(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (key == null)
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first.intValue() == key.intValue()) && eq(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) == null?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value equal to <code>null</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value equal to <code>null</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] == null)
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex 
      @                                 ==> a[i] == null); 
      @*/
    public static /*@ pure @*/ boolean eqnull(java_v.lang_v.Integer[] a, int fromIndex, int toIndex)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first == null) && eqnull(a, fromIndex + 1, toIndex);
        }
    }

    // Does a[fromIndex..toIndex) != key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value not equal to <code>key</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>int</code> value
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value not equal to <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] != key)
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] != key); 
      @*/
    public static /*@ pure @*/ boolean neq(int[] a, int fromIndex, int toIndex, int key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            int first = a[fromIndex];
            return (first != key) && neq(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) != key?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value not equal to <code>key</code>.
     * 
     * @param a   an array without <code>null</code> values over <code>[fromIndex..toIndex)</code>
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @param key   an <code>Integer</code> value, not <code>null</code>
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value not equal to  <code>key</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.require   <code>nonnull(a, fromIndex, toIndex)</code>
     * @dm.require   <code>key != null</code>
     * @dm.ensure    \result <==> (\forall int i; 
                         fromIndex <= i && i < toIndex ==> a[i].intValue() != key.intValue())
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @ requires nonnull(a, fromIndex, toIndex);
      @ requires key != null;
      @ 
      @ ensures \result <==> 
      @        (\forall int i; fromIndex <= i && i < toIndex ==> a[i].intValue() != key.intValue()); 
      @*/
    public static /*@ pure @*/ boolean neq(java_v.lang_v.Integer[] a, int fromIndex, int toIndex, 
                                           java_v.lang_v.Integer key)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (!nonnull(a, fromIndex, toIndex))
            throw new RuntimeException();
        if (key == null)
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first.intValue() != key.intValue()) && neq(a, fromIndex + 1, toIndex, key);
        }
    }

    // Does a[fromIndex..toIndex) != null?
    /**
     * Returns <code>true</code> iff all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
       have value not equal to <code>null</code>.
     * 
     * @param a   an array
     * @param fromIndex   a lower <code>int</code> index
     * @param toIndex   an upper <code>int</code> index
     * @return    <code>true</code> if all elements of <code>a</code> over <code>[fromIndex..toIndex)</code> 
                  have value not equal to <code>null</code>, 
                  otherwise <code>false</code>
     * @throws RuntimeException if the precondition fails
     * @dm.require   <code>a != null</code>
     * @dm.require   <code>within(a, fromIndex, toIndex)</code>
     * @dm.ensure    \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] != null)
     */
    /*@ 
      @ requires a != null;
      @ requires within(a, fromIndex, toIndex);
      @
      @ ensures \result <==> (\forall int i; fromIndex <= i && i < toIndex ==> a[i] != null); 
      @*/
    public static /*@ pure @*/ boolean nonnull(java_v.lang_v.Integer[] a, int fromIndex, int toIndex)
    {
        if (a == null)
            throw new RuntimeException();
        if (!within(a, fromIndex, toIndex))
            throw new RuntimeException();

        if (fromIndex >= toIndex) {
            return true;
        } else {
            java_v.lang_v.Integer first = a[fromIndex];
            return (first != null) && nonnull(a, fromIndex + 1, toIndex);
        }
    }

}
