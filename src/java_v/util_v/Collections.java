/*
 * COPYRIGHT NOTICE FOR JAVA CODE:
 *
 * Copyright 1997-2007 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */
 
 
/*
 * COPYRIGHT NOTICE FOR JML ANNOTATIONS:
 *
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * The JML annotations are free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 */
 


package java_v.util_v;

import java_v.theories.TLists;
import java_v.theories.TArrays;


public class Collections {

    /*@ 
      @ requires l != null;
      @ requires TLists.neq(l, 0, l.size(), null);
      @
      @ ensures l.size() == \old(l.size());
      @ ensures TLists.neq(l, 0, l.size(), null);
      @ ensures TLists.sorted(l, 0, l.size());
      @*/
    public static void sort(ArrayList l) {
        java_v.lang_v.Integer[] a = l.toArray();
        Arrays.sortInteger(a);
        //@ assume a.length == l.size();
        //@ assume TArrays.sorted(a, 0, a.length);
        //@ assume TArrays.neq(a, 0, a.length, null);
        l.clear();

        int ic = 0;
        //@ loop_invariant 0 <= ic && ic <= a.length;
        //@ loop_invariant ic == l.size();
        //@ loop_invariant (\forall int j; 0 <= j && j < a.length ==> a[j] == \old(a[j])); 
        //@ loop_invariant (\forall int j; 0 <= j && j < ic ==> a[j] == l.getElementData()[j]); 
        for (; ic < a.length; ic++) {
            java_v.lang_v.Integer elem = a[ic];
            l.add(elem);
        }
    }

    /**
     * Reverses the order of the elements in the specified list.<p>
     *
     * This method runs in linear time.
     *
     * @param  list the list whose elements are to be reversed.
     * @throws UnsupportedOperationException if the specified list or
     *         its list-iterator does not support the <tt>set</tt> operation.
     */
    /*@ 
      @ requires list != null;
      @
      @ assignable list.elementData[*];
      @
      @ ensures (\forall int j; 0 <= j && j < list.size() 
      @                   ==> list.elementData[j] == \old(list.elementData[list.size() - 1 - j]));
      @*/
    public static void reverse(ArrayList list) {
        int size = list.size();
        int mid = size / 2;
        int ic = 0;

        //@ loop_invariant 0 <= ic && ic <= mid;
        //@ loop_invariant (\forall int j; ic <= j && j < size - ic; list.getIndex(j) == \old(list.getIndex(j)));
        //@ loop_invariant (\forall int j; 0 <= j && j < ic; list.getIndex(j) == \old(list.getIndex(size - 1 - j)));
        //@ loop_invariant (\forall int j; 0 <= j && j < ic; list.getIndex(size - 1 - j) == \old(list.getIndex(j)));
        for (; ic < mid; ic++) {
            swap(list, ic, size - 1 - ic);
        }
    }

    /**
     * Swaps the elements at the specified positions in the specified list.
     * (If the specified positions are equal, invoking this method leaves
     * the list unchanged.)
     *
     * @param list The list in which to swap elements.
     * @param i the index of one element to be swapped.
     * @param j the index of the other element to be swapped.
     * @throws IndexOutOfBoundsException if either <tt>i</tt> or <tt>j</tt>
     *         is out of range (i &lt; 0 || i &gt;= list.size()
     *         || j &lt; 0 || j &gt;= list.size()).
     * @since 1.4
     */

    /*@
      @ requires list != null;
      @ requires 0 <= i && i < list.size;
      @ requires 0 <= j && j < list.size;
      @
      @ assignable list.elementData[i], list.elementData[j];
      @
      @ ensures list.elementData[i] == \old(list.elementData[j]);
      @ ensures list.elementData[j] == \old(list.elementData[i]);
      @ ensures (\forall int k; 0 <= k && k < list.size & k != i && k != j 
      @                            ==> list.elementData[k] == \old(list.elementData[k]));
      @*/
    public static void swap(ArrayList list, int i, int j) {
        java_v.lang_v.Integer val_i = list.getIndex(i);
        java_v.lang_v.Integer val_j = list.setIndex(j, val_i);
        list.setIndex(i, val_j);
    }

    /*@ 
      @ requires list != null;
      @
      @ assignable list.elementData[*];
      @
      @ ensures oldVal == newVal ==> 
      @     (\forall int j; 0 <= j && j < list.size() ==> list.elementData[j] == \old(list.elementData[j]));
      @ ensures oldVal != newVal ==> TLists.neq(list, 0, list.size(), oldVal);
      @*/
    public static void replaceAll(ArrayList list, java_v.lang_v.Integer oldVal,
                                  java_v.lang_v.Integer newVal) {

        if (oldVal == newVal)
            return;

        int ic = 0;
        int size = list.size();

        //@ loop_invariant 0 <= ic && ic <= size;
        //@ loop_invariant TLists.neq(list, 0, ic, oldVal); 
        for (; ic < size; ic++) {
            java_v.lang_v.Integer val = list.getIndex(ic);
            if (val == oldVal) {
                list.setIndex(ic, newVal);
            }
        }
    }

    public static void rotate1(ArrayList list, int distance) {
        int size = list.size();
        if (size == 0)
            return;
        distance = distance % size;
        if (distance < 0)
            distance += size;
        if (distance == 0)
            return;

        checked_rotate1(list, distance);
    }

    /*@ 
      @ requires list != null;
      // Based on how `checked_rotate1' is called within `rotate1':
      @ requires list.size() > 0 && 0 < distance && distance < list.size();
      @
      @ ensures list.size() == \old(list.size());
      @ ensures (\forall int j; distance <= j && j < list.size() 
      @                  ==> list.elementData[j - distance] == \old(list.elementData[j]));
      @ ensures (\forall int j; 0 <= j && j < distance ==> 
      @                  list.elementData[j + list.size() - distance] == \old(list.elementData[j]));
      @*/
    private static void checked_rotate1(ArrayList list, final int distance) {
        java_v.lang_v.Integer[] intArray = list.toArray();
        list.clear();
        int ic = distance;
        //@ loop_invariant distance <= ic && ic <= intArray.length;
        //@ loop_invariant list.size == ic - distance;
        //@ loop_invariant (\forall int j; 0 <= j && j < intArray.length ==> intArray[j] == \old(intArray[j]));
        //@ loop_invariant (\forall int j; distance <= j && j < ic  ==> list.getIndex(j - distance) == intArray[j]);
        for (; ic < intArray.length; ic++) {
            list.add(intArray[ic]);
        }

        int jc = 0;
        //@ loop_invariant 0 <= jc && jc <= distance;
        //@ loop_invariant list.size == jc + intArray.length - distance;
        //@ loop_invariant (\forall int j; 0 <= j && j < intArray.length  ==> intArray[j] == \old(intArray[j]));
        //@ loop_invariant (\forall int j; distance <= j && j < intArray.length ==> list.getIndex(j - distance) == intArray[j]); 
        //@ loop_invariant (\forall int j; 0 <= j && j < jc ==> list.getIndex(j + intArray.length - distance) == intArray[j]); 
        for (; jc < distance; jc++) {
            list.add(intArray[jc]);
        }
    }

    public static void rotate0(ArrayList list, int distance) {
        int size = list.size();
        if (size == 0)
            return;
        distance = distance % size;
        if (distance < 0)
            distance += size;
        if (distance == 0)
            return;

        checked_rotate0(list, distance);
    }

    /*@ requires list != null;
    // Based on how `checked_rotate0' is called within `rotate0':
    @ requires list.size() > 0 && 0 < distance && distance < list.size();
    @ ensures list.size() == \old(list.size());
    @ ensures (\forall int j; distance <= j && j < list.size() 
    @                  ==> list.elementData[j - distance] == \old(list.elementData[j]));
    @ ensures (\forall int j; 0 <= j && j < distance ==> 
    @                  list.elementData[j + list.size() - distance] == \old(list.elementData[j]));
    @*/
    private static void checked_rotate0(ArrayList list, int distance) {
        int size = list.size();
        int cycleStart = 0, nMoved = 0;

        // For complete loop invariants and proofs of this method see:
        // http://arxiv.org/abs/1406.5453

        //@ loop_invariant 0 <= nMoved && nMoved <= size;
        //@ loop_invariant 0 <= cycleStart && cycleStart < size;
        for (; nMoved != size; cycleStart++) {
            java_v.lang_v.Integer displaced = list.getIndex(cycleStart);
            int ic = cycleStart;

            // Each execution of this inner loop iterates exactly gcd(size, distance) times
            // and hence moves the same number of elements.
            //@ loop_invariant 0 <= nMoved && nMoved <= size;
            //@ loop_invariant 0 <= ic && ic < size;
            do {
                ic += distance;
                if (ic >= size) {
                    ic -= size;
                }
                displaced = list.setIndex(ic, displaced);
                nMoved++;
            } while (ic != cycleStart);

        }
    }

    /**
     * Returns a synchronized (thread-safe) list backed by the specified
     * list.  In order to guarantee serial access, it is critical that
     * <strong>all</strong> access to the backing list is accomplished
     * through the returned list.<p>
     *
     * It is imperative that the user manually synchronize on the returned
     * list when iterating over it:
     * <pre>
     *  List list = Collections.synchronizedList(new ArrayList());
     *      ...
     *  synchronized(list) {
     *      Iterator i = list.iterator(); // Must be in synchronized block
     *      while (i.hasNext())
     *          foo(i.next());
     *  }
     * </pre>
     * Failure to follow this advice may result in non-deterministic behavior.
     *
     * <p>The returned list will be serializable if the specified list is
     * serializable.
     *
     * @param  list the list to be "wrapped" in a synchronized list.
     * @return a synchronized view of the specified list.
     */
    public static List synchronizedList(List list) {
        return (list instanceof RandomAccess ? new SynchronizedRandomAccessList(
                                                                                list) : new SynchronizedList(list));
    }

    static List synchronizedList(List list, Object mutex) {
        return (list instanceof RandomAccess ? new SynchronizedRandomAccessList(
                                                                                list, mutex) : new SynchronizedList(list, mutex));
    }

    /**
     * @serial include
     */
    static class SynchronizedCollection implements Collection {

        final Collection c; // Backing Collection
        final Object mutex; // Object on which to synchronize

        SynchronizedCollection(Collection c) {
            if (c == null)
                throw new NullPointerException();
            this.c = c;
            mutex = this;
        }

        SynchronizedCollection(Collection c, Object mutex) {
            this.c = c;
            this.mutex = mutex;
        }

        public int size() {
            synchronized (mutex) {
                return c.size();
            }
        }

        public boolean isEmpty() {
            synchronized (mutex) {
                return c.isEmpty();
            }
        }

        public boolean contains(java_v.lang_v.Integer o) {
            synchronized (mutex) {
                return c.contains(o);
            }
        }

        public java_v.lang_v.Integer[] toArray() {
            synchronized (mutex) {
                return c.toArray();
            }
        }

        public Iterator iterator() {
            return c.iterator(); // Must be manually synched by user!
        }

        public boolean add(java_v.lang_v.Integer e) {
            synchronized (mutex) {
                return c.add(e);
            }
        }

        public boolean remove(java_v.lang_v.Integer o) {
            synchronized (mutex) {
                return c.remove(o);
            }
        }

        public boolean containsAll(Collection coll) {
            synchronized (mutex) {
                return c.containsAll(coll);
            }
        }

        public boolean addAll(Collection coll) {
            synchronized (mutex) {
                return c.addAll(coll);
            }
        }

        public boolean removeAll(Collection coll) {
            synchronized (mutex) {
                return c.removeAll(coll);
            }
        }

        public boolean retainAll(Collection coll) {
            synchronized (mutex) {
                return c.retainAll(coll);
            }
        }

        public void clear() {
            synchronized (mutex) {
                c.clear();
            }
        }
    }

    /**
     * @serial include
     */
    static class SynchronizedList extends SynchronizedCollection implements
                                                                     List {

        final List list;

        SynchronizedList(List list) {
            super(list);
            this.list = list;
        }

        SynchronizedList(List list, Object mutex) {
            super(list, mutex);
            this.list = list;
        }

        public java_v.lang_v.Integer get(int index) {
            synchronized (mutex) {
                return list.get(index);
            }
        }

        public java_v.lang_v.Integer set(int index, java_v.lang_v.Integer element) {
            synchronized (mutex) {
                return list.set(index, element);
            }
        }

        public void add(int index, java_v.lang_v.Integer element) {
            synchronized (mutex) {
                list.add(index, element);
            }
        }

        public java_v.lang_v.Integer removeIndex(int index) {
            synchronized (mutex) {
                return list.removeIndex(index);
            }
        }

        public int indexOf0(java_v.lang_v.Integer o) {
            synchronized (mutex) {
                return list.indexOf0(o);
            }
        }

        public int lastIndexOf(java_v.lang_v.Integer o) {
            synchronized (mutex) {
                return list.lastIndexOf(o);
            }
        }

        public boolean addAll(int index, Collection c) {
            synchronized (mutex) {
                return list.addAll(index, c);
            }
        }

        public ListIterator listIterator() {
            return list.listIterator(); // Must be manually synched by user
        }

        public ListIterator listIterator(int index) {
            return list.listIterator(index); // Must be manually synched by user
        }

        public List subList(int fromIndex, int toIndex) {
            synchronized (mutex) {
                return new SynchronizedList(list.subList(fromIndex, toIndex),
                                            mutex);
            }
        }

    }

    /**
     * @serial include
     */
    static class SynchronizedRandomAccessList extends SynchronizedList
        implements RandomAccess {

        SynchronizedRandomAccessList(List list) {
            super(list);
        }

        SynchronizedRandomAccessList(List list, Object mutex) {
            super(list, mutex);
        }

        public List subList(int fromIndex, int toIndex) {
            synchronized (mutex) {
                return new SynchronizedRandomAccessList(list.subList(fromIndex,
                                                                     toIndex), mutex);
            }
        }
    }

    /**
     * The only purpose of this method is allowing 
     * Chicory to access the private fields of this class.
     * 
     */
    private void unreachable_method() {
    }

}
