/*
 * COPYRIGHT NOTICE FOR JAVA CODE:
 *
 * Copyright 1997-2007 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */
 
 
/*
 * COPYRIGHT NOTICE FOR JML ANNOTATIONS:
 *
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * The JML annotations are free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 */
 


package java_v.util_v;

import java_v.theories.TLists;


public class Vector extends AbstractList {

    //@ invariant elementData != null;
    //@ invariant (\forall int i; elementCount <= i && i < elementData.length ==> elementData[i] == null);
    //@ invariant elementData.owner == this;
    private /*@ spec_public @*/ java_v.lang_v.Integer[] elementData;

    //@ invariant 0 <= elementCount && elementCount <= elementData.length;
    private /*@ spec_public @*/ int elementCount;

    private /*@ spec_public @*/ int modCount;

    //@ invariant capacityIncrement >= 0;
    protected /*@ spec_public @*/ int capacityIncrement;

    /**
     * Constructs an empty vector with the specified initial capacity and
     * capacity increment.
     *
     * @param   initialCapacity     the initial capacity of the vector
     * @param   capacityIncrement   the amount by which the capacity is
     *                              increased when the vector overflows
     * @throws IllegalArgumentException if the specified initial capacity
     *         is negative
     */
    public Vector(int initialCapacity, int capacityIncrement) {
        super();
        if (initialCapacity < 0)
            throw new java_v.lang_v.IllegalArgumentException();
        this.elementData = new java_v.lang_v.Integer[initialCapacity];
        this.capacityIncrement = capacityIncrement;
    }

    /**
     * Constructs an empty vector with the specified initial capacity and
     * with its capacity increment equal to zero.
     *
     * @param   initialCapacity   the initial capacity of the vector
     * @throws IllegalArgumentException if the specified initial capacity
     *         is negative
     */
    public Vector(int initialCapacity) {
        this(initialCapacity, 0);
    }

    /**
     * Constructs a vector containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.
     *
     * @param c the collection whose elements are to be placed into this
     *       vector
     * @throws NullPointerException if the specified collection is null
     * @since   1.2
     */
    public Vector(Collection c) {
        elementData = c.toArray();
        elementCount = elementData.length;
    }

    /**
     * Copies the components of this vector into the specified array.
     * The item at index {@code k} in this vector is copied into
     * component {@code k} of {@code anArray}.
     *
     * @param  anArray the array into which the components get copied
     * @throws NullPointerException if the given array is null
     * @throws IndexOutOfBoundsException if the specified array is not
     *         large enough to hold all the components of this vector
     * @throws ArrayStoreException if a component of this vector is not of
     *         a runtime type that can be stored in the specified array
     * @see #toArray(Object[])
     */
    public synchronized void copyInto(java_v.lang_v.Integer[] anArray) {
        java_v.lang_v.System.arraycopyInteger(elementData, 0, anArray, 0,
                                              elementCount);
    }

    /**
     * Trims the capacity of this vector to be the vector's current
     * size. If the capacity of this vector is larger than its current
     * size, then the capacity is changed to equal the size by replacing
     * its internal data array, kept in the field {@code elementData},
     * with a smaller one. An application can use this operation to
     * minimize the storage of a vector.
     */
    public synchronized void trimToSize() {
        modCount++;
        int oldCapacity = elementData.length;
        if (elementCount < oldCapacity) {
            elementData = Arrays.copyOf(elementData, elementCount);
        }
    }
    
    /**
     * Increases the capacity of this vector, if necessary, to ensure
     * that it can hold at least the number of components specified by
     * the minimum capacity argument.
     *
     * <p>If the current capacity of this vector is less than
     * {@code minCapacity}, then its capacity is increased by replacing its
     * internal data array, kept in the field {@code elementData}, with a
     * larger one.  The size of the new data array will be the old size plus
     * {@code capacityIncrement}, unless the value of
     * {@code capacityIncrement} is less than or equal to zero, in which case
     * the new capacity will be twice the old capacity; but if this new size
     * is still smaller than {@code minCapacity}, then the new capacity will
     * be {@code minCapacity}.
     *
     * @param minCapacity the desired minimum capacity
     */
    public synchronized void ensureCapacity(int minCapacity) {
        modCount++;
        ensureCapacityHelper(minCapacity);
    }

    /**
     * Constructs an empty vector so that its internal data array
     * has size {@code 10} and its standard capacity increment is
     * zero.
     */
    public Vector() {
        this(10);
    }
    
    /**
     * Removes all components from this vector and sets its size to zero.
     *
     * <p>This method is identical in functionality to the {@link #clear}
     * method (which is part of the {@link List} interface).
     */
    /*@ 
      @ assignable modCount, elementData[*], elementCount;
      @
      @ ensures elementCount == 0;
      @ ensures TLists.eq(elementData, 0, \old(elementCount), null); 
      @ ensures TLists.eq(elementData, \old(elementCount), elementData.length, null);
      @*/
    public synchronized void removeAllElements() {
        modCount++;
        // Let gc do its work

        int ic = 0;

        //@ loop_invariant 0 <= ic && ic <= elementCount;
        //@ loop_invariant TLists.eq(elementData, 0, ic, null);
        //@ loop_invariant TLists.eq(elementData, \old(elementCount), elementData.length, null);
        //@ loop_invariant (\forall Vector v; v != null && v != this ==> (\forall int j; 0 <= j && j < v.elementData.length ==> v.elementData[j] == \old(v.elementData[j]))); 
        //@ loop_invariant (\forall Vector v; v != null && v != this; (\forall int i; 0 <= i && i< v.elementData.length; v.elementData[i] == \old(v.elementData[i]))); // framing
        //@ loop_invariant (\forall Vector v; v != null && v != this; v.elementCount == \old(v.elementCount)); // framing
        //@ loop_invariant (\forall Vector v ; v != null && v != this; v.modCount == \old(v.modCount)); //framing // framing
        for (; ic < elementCount; ic++) {
            elementData[ic] = null;
        }

        elementCount = 0;
    }

    /**
     * Returns the index of the first occurrence of the specified element in
     * this vector, searching forwards from {@code index}, or returns -1 if
     * the element is not found.
     * More formally, returns the lowest index {@code i} such that
     * <tt>(i&nbsp;&gt;=&nbsp;index&nbsp;&amp;&amp;&nbsp;(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i))))</tt>,
     * or -1 if there is no such index.
     *
     * @param value element to search for
     * @param index index to start searching from
     * @return the index of the first occurrence of the element in
     *         this vector at position {@code index} or later in the vector;
     *         {@code -1} if the element is not found.
     * @throws IndexOutOfBoundsException if the specified index is negative
     * @see     Object#equals(Object)
     */
    // Called "indexOf" in http://arxiv.org/abs/1407.5286
    /*@ normal_behavior
      @ requires 0 <= index && index <= elementCount;
      @ ensures \result == -1 <==> !TLists.in(elementData, index, elementCount, value);
      @ ensures \result != -1 ==> index <= \result && \result < elementCount;
      @ ensures \result != -1 ==> elementData[\result] == value && 
      @                                  !TLists.in(elementData, index, \result, value);
      @
      @ also exceptional_behavior
      @ requires index < 0 || index > elementCount;
      @ signals (java_v.lang_v.IndexOutOfBoundsException ex) index < 0 || index > elementCount;
      @*/
    public /*@ pure @*/ synchronized int indexOf1(java_v.lang_v.Integer value,
                                                  int index) 
        throws java_v.lang_v.IndexOutOfBoundsException {
        if (index < 0 || index > elementCount)
            throw new java_v.lang_v.IndexOutOfBoundsException();

        if (value == null) {
            int ic = index;
            //@ loop_invariant index <= ic && ic <= elementCount;
            //@ loop_invariant !TLists.in(elementData, index, ic, value);
            for (; ic < elementCount; ic++) {
                if (elementData[ic] == null) {
                    return ic;
                }
            }
        } else {
            int ic = index;
            //@ loop_invariant index <= ic && ic <= elementCount;
            //@ loop_invariant !TLists.in(elementData, index, ic, value);
            for (; ic < elementCount; ic++) {
                if (value == elementData[ic]) {
                    return ic;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the index of the last occurrence of the specified element in
     * this vector, searching backwards from {@code index}, or returns -1 if
     * the element is not found.
     * More formally, returns the highest index {@code i} such that
     * <tt>(i&nbsp;&lt;=&nbsp;index&nbsp;&amp;&amp;&nbsp;(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i))))</tt>,
     * or -1 if there is no such index.
     *
     * @param value element to search for
     * @param index index to start searching backwards from
     * @return the index of the last occurrence of the element at position
     *         less than or equal to {@code index} in this vector;
     *         -1 if the element is not found.
     * @throws IndexOutOfBoundsException if the specified index is greater
     *         than or equal to the current size of this vector
     */
    // Called "lastIndexOf" in http://arxiv.org/abs/1407.5286
    /*@ normal_behavior
      @ requires 0 <= index && index < elementCount;
      @ ensures \result == -1 <==> !TLists.in(elementData, 0, index + 1, value);
      @ ensures \result != -1 ==> 0 <= \result && \result <= index;
      @ ensures \result != -1 ==> elementData[\result] == value && 
      @                                  !TLists.in(elementData, \result + 1, index + 1, value);
      @
      @ also exceptional_behavior
      @ requires index < 0 || index >= elementCount;
      @ signals (java_v.lang_v.IndexOutOfBoundsException ex) index < 0 || index >= elementCount;
      @*/
    public /*@ pure @*/ synchronized int lastIndexOf1(java_v.lang_v.Integer value,
                                                      int index) 
        throws java_v.lang_v.IndexOutOfBoundsException {
        if (index < 0 || index >= elementCount)
            throw new java_v.lang_v.IndexOutOfBoundsException();
       
        if (value == null) {
            int ic = index;
            //@ loop_invariant -1 <= ic && ic <= index;
            //@ loop_invariant !TLists.in(elementData, ic + 1, index + 1, value);
            for (; ic >= 0; ic--) {
                if (elementData[ic] == null) {
                    return ic;
                }
            }
        } else {
            int ic = index;
            //@ loop_invariant -1 <= ic && ic <= index;
            //@ loop_invariant !TLists.in(elementData, ic + 1, index + 1, value);
            for (; ic >= 0; ic--) {
                if (value == elementData[ic]) {
                    return ic;
                }
            }
        }
        return -1;
    }

    /**
     * Sets the size of this vector. If the new size is greater than the
     * current size, new {@code null} items are added to the end of
     * the vector. If the new size is less than the current size, all
     * components at index {@code newSize} and greater are discarded.
     *
     * @param  newSize   the new size of this vector
     * @throws ArrayIndexOutOfBoundsException if the new size is negative
     */
    /*@ normal_behavior
      @ requires newSize >= 0;
      @ assignable modCount, elementData, elementData[*], elementCount;
      @ ensures elementCount == newSize;
      @ ensures (\forall int j; 0 <= j && j < (newSize <= \old(elementCount) ? newSize : \old(elementCount)) 
      @                       ==> elementData[j] == \old(elementData[j]));
      @ ensures newSize < \old(elementCount) ==> TLists.eq(elementData, newSize, elementData.length, null);
      @
      @ also exceptional_behavior
      @ requires newSize < 0;
      @ signals (java_v.lang_v.IndexOutOfBoundsException ex) newSize < 0;
      @*/
    public synchronized void setSize(int newSize)
        throws java_v.lang_v.IndexOutOfBoundsException {
        if (newSize < 0)
            throw new java_v.lang_v.IndexOutOfBoundsException();

        modCount++;
        if (newSize > elementCount) {
            ensureCapacityHelper(newSize);
            // Assuming, so I don't need a full specification of `ensureCapacityHelper'.
            //@ assume (\forall int i; 0 <= i && i < elementCount ==> elementData[i] == \old(elementData[i]));
            //@ assume TLists.eq(elementData, elementCount, newSize, null);
        } else {

            int ic = newSize;

            //@ loop_invariant newSize <= ic;
            //@ loop_invariant ic <= elementCount;
            //@ loop_invariant TLists.eq(elementData, newSize, ic, null);
            //@ loop_invariant (\forall int j; 0 <= j && j < newSize ==> elementData[j] == \old(elementData[j])); 
            //@ loop_invariant (\forall int j; elementCount <= j && j < elementData.length ==> elementData[j] == \old(elementData[j]));
            //@ loop_invariant (\forall Vector v; v != null && v != this ==> (\forall int j; 0 <= j && j < v.elementData.length ==> v.elementData[j] == \old(v.elementData[j])));
            //@ loop_invariant (\forall Vector v; v != null && v != this; (\forall int i; 0 <= i && i < v.elementData.length; v.elementData[i] == \old(v.elementData[i]))); // framing
            //@ loop_invariant (\forall Vector v; v != null && v != this; v.elementCount == \old(v.elementCount)); // framing
            //@ loop_invariant (\forall Vector v; v != null && v != this; v.modCount == \old(v.modCount)); // framing
            //@ loop_invariant (\forall Vector v; v != null && v != this; v.elementData == \old(v.elementData)); // framing
            for (; ic < elementCount; ic++) {
                elementData[ic] = null;
            }
        }
        elementCount = newSize;
    }

    /**
     * This implements the unsynchronized semantics of ensureCapacity.
     * Synchronized methods in this class can internally call this
     * method for ensuring capacity without incurring the cost of an
     * extra synchronization.
     *
     * @see #ensureCapacity(int)
     */
    /*@ 
      @ assignable elementData; 
      @
      @ ensures elementData.length >= \old(elementData.length); 
      @ ensures elementData.length >= minCapacity; 
      @*/
    private void ensureCapacityHelper(int minCapacity) {
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            java_v.lang_v.Integer[] oldData = elementData;
            int newCapacity = (capacityIncrement > 0) ? (oldCapacity + capacityIncrement)
                : (oldCapacity * 2);
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            elementData = Arrays.copyOf(elementData, newCapacity);
        }
    }

    protected synchronized void removeRange(int fromIndex, int toIndex) {
        removeRange0(fromIndex, toIndex);
    }

    /**
     * Removes from this list all of the elements whose index is between
     * {@code fromIndex}, inclusive, and {@code toIndex}, exclusive.
     * Shifts any succeeding elements to the left (reduces their index).
     * This call shortens the list by {@code (toIndex - fromIndex)} elements.
     * (If {@code toIndex==fromIndex}, this operation has no effect.)
     */
    /*@ 
      @ requires 0 <= fromIndex && fromIndex <= toIndex && toIndex < elementCount;
      @
      @ assignable elementCount, elementData[*], modCount;
      @
      @ ensures elementCount == \old(elementCount) - (toIndex - fromIndex);
      @ ensures (\forall int i; 0 <= i && i < fromIndex ==> elementData[i] == \old(elementData[i]));
      @ ensures (\forall int i; 0 <= i && i < (\old(elementCount) - toIndex)
      @                            ==> elementData[fromIndex + i] == \old(elementData[toIndex + i]));
      // caf: I find the following formulation clearer, but ESC/Java 2 cannot reason well about it:
      // forall fromIndex <= i < elementCount: elementData[i] == \old(elementData[toIndex + i - fromIndex])
      @*/
    protected synchronized void removeRange0(int fromIndex, int toIndex) {
        modCount++;
        int numMoved = elementCount - toIndex;
        java_v.lang_v.System.arraycopyInteger(elementData, toIndex, elementData,
                                              fromIndex, numMoved);

        // Let gc do its work
        int oldElementCount = elementCount;
        int newElementCount = oldElementCount - (toIndex - fromIndex);

        //@ loop_invariant newElementCount <= elementCount;
        //@ loop_invariant elementCount <= oldElementCount;
        //@ loop_invariant (\forall int i; 0 <= i && i < newElementCount ==> elementData[i] == \old(elementData[i])); 
        //@ loop_invariant (\forall int i; oldElementCount <= i && i < elementData.length ==> elementData[i] == \old(elementData[i])); 
        //@ loop_invariant TLists.eq(elementData, elementCount, oldElementCount, null);
        //@ loop_invariant (\forall Vector v; v != null && v != this ==> (\forall int j; 0 <= j && j < v.elementData.length ==> v.elementData[j] == \old(v.elementData[j])));
        //@ loop_invariant (\forall Vector v; v != null && v != this ==> v.elementCount == \old(v.elementCount));
        //@ loop_invariant (\forall Vector v; v != null && v != this; (\forall int i; 0 <= i && i < v.elementData.length; v.elementData[i] == \old(v.elementData[i]))); // framing
        //@ loop_invariant (\forall Vector v; v != null && v != this; v.elementCount == \old(v.elementCount)); // framing
        //@ loop_invariant (\forall Vector v; v != null && v != this; v.modCount == \old(v.modCount)); // framing
        while (elementCount != newElementCount) {
            elementData[--elementCount] = null;
        }
    }

    /*@ also
      @ assignable elementData[*];
      @
      @ ensures (\forall int i; 0 <= i && i < elementData.length; elementData[i] == \old(elementData[i]));
      @ ensures \result.length == elementCount;
      @ ensures (\forall int i; 0 <= i && i < \result.length; \result[i] == elementData[i]);
      @*/
    public synchronized java_v.lang_v.Integer[] toArray() {
        return java_v.util_v.Arrays.copyOf(elementData, elementCount);
    }

    /**
     * Returns {@code true} if this vector contains the specified element.
     * More formally, returns {@code true} if and only if this vector
     * contains at least one element {@code e} such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param value element whose presence in this vector is to be tested
     * @return {@code true} if this vector contains the specified element
     */

    /*@ also
      @ ensures (\result <==> (\exists int i; 0 <= i && i < elementCount && value == elementData[i]));
      @*/
    public/*@pure@*/boolean contains(java_v.lang_v.Integer value) {
        int indexOf = indexOf1(value, 0);
        return indexOf >= 0;
    }

    /**
     * Returns the current capacity of this vector.
     *
     * @return  the current capacity (the length of its internal
     *          data array, kept in the field {@code elementData}
     *          of this vector)
     */
    public synchronized int capacity() {
        return elementData.length;
    }

    /**
     * Returns the number of components in this vector.
     *
     * @return  the number of components in this vector
     */
    public synchronized int size() {
        return elementCount;
    }

    /**
     * Tests if this vector has no components.
     *
     * @return  {@code true} if and only if this vector has
     *          no components, that is, its size is zero;
     *          {@code false} otherwise.
     */
    public synchronized boolean isEmpty() {
        return elementCount == 0;
    }

    //   /**
    //    * Returns an enumeration of the components of this vector. The
    //    * returned {@code Enumeration} object will generate all items in
    //    * this vector. The first item generated is the item at index {@code 0},
    //    * then the item at index {@code 1}, and so on.
    //    *
    //    * @return  an enumeration of the components of this vector
    //    * @see     Iterator
    //    */
    //   public Enumeration elements() {
    //      return new Enumeration() {
    //         int count = 0;
    //
    //         public boolean hasMoreElements() {
    //            return count < elementCount;
    //         }
    //
    //         public Integer nextElement() {
    //            synchronized (Vector.this) {
    //               if (count < elementCount) {
    //                  return elementData(count++);
    //               }
    //            }
    //            throw new NoSuchElementException();
    //         }
    //      };
    //   }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this vector, or -1 if this vector does not contain the element.
     * More formally, returns the lowest index {@code i} such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     *
     * @param o element to search for
     * @return the index of the first occurrence of the specified element in
     *         this vector, or -1 if this vector does not contain the element
     */
    public int indexOf0(java_v.lang_v.Integer o) {
        return indexOf1(o, 0);
    }

    /**
     * Returns the index of the last occurrence of the specified element
     * in this vector, or -1 if this vector does not contain the element.
     * More formally, returns the highest index {@code i} such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     *
     * @param o element to search for
     * @return the index of the last occurrence of the specified element in
     *         this vector, or -1 if this vector does not contain the element
     */
    public synchronized int lastIndexOf0(java_v.lang_v.Integer o) {
        return lastIndexOf1(o, elementCount - 1);
    }

    /**
     * Returns the component at the specified index.
     *
     * <p>This method is identical in functionality to the {@link #get(int)}
     * method (which is part of the {@link List} interface).
     *
     * @param      index   an index into this vector
     * @return     the component at the specified index
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */
    public synchronized java_v.lang_v.Integer elementAt(int index) {
        if (index >= elementCount) {
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();
        }

        return elementData(index);
    }

    /**
     * Returns the first component (the item at index {@code 0}) of
     * this vector.
     *
     * @return     the first component of this vector
     * @throws NoSuchElementException if this vector has no components
     */
    public synchronized java_v.lang_v.Integer firstElement() {
        if (elementCount == 0) {
            throw new NoSuchElementException();
        }
        return elementData(0);
    }

    /**
     * Returns the last component of the vector.
     *
     * @return  the last component of the vector, i.e., the component at index
     *          <code>size()&nbsp;-&nbsp;1</code>.
     * @throws NoSuchElementException if this vector is empty
     */
    public synchronized java_v.lang_v.Integer lastElement() {
        if (elementCount == 0) {
            throw new NoSuchElementException();
        }
        return elementData(elementCount - 1);
    }

    /**
     * Sets the component at the specified {@code index} of this
     * vector to be the specified object. The previous component at that
     * position is discarded.
     *
     * <p>The index must be a value greater than or equal to {@code 0}
     * and less than the current size of the vector.
     *
     * <p>This method is identical in functionality to the
     * {@link #set(int, Object) set(int, E)}
     * method (which is part of the {@link List} interface). Note that the
     * {@code set} method reverses the order of the parameters, to more closely
     * match array usage.  Note also that the {@code set} method returns the
     * old value that was stored at the specified position.
     *
     * @param      obj     what the component is to be set to
     * @param      index   the specified index
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */
    public synchronized void setElementAt(java_v.lang_v.Integer obj, int index) {
        if (index >= elementCount) {
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();
        }
        elementData[index] = obj;
    }

    /**
     * Deletes the component at the specified index. Each component in
     * this vector with an index greater or equal to the specified
     * {@code index} is shifted downward to have an index one
     * smaller than the value it had previously. The size of this vector
     * is decreased by {@code 1}.
     *
     * <p>The index must be a value greater than or equal to {@code 0}
     * and less than the current size of the vector.
     *
     * <p>This method is identical in functionality to the {@link #removeIndex(int)}
     * method (which is part of the {@link List} interface).  Note that the
     * {@code remove} method returns the old value that was stored at the
     * specified position.
     *
     * @param      index   the index of the object to remove
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */
    public synchronized void removeElementAt(int index) {
        modCount++;
        if (index >= elementCount) {
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();
        } else if (index < 0) {
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();
        }
        int j = elementCount - index - 1;
        if (j > 0) {
            java_v.lang_v.System.arraycopyInteger(elementData, index + 1,
                                                  elementData, index, j);
        }
        elementCount--;
        elementData[elementCount] = null; /* to let gc do its work */
    }

    /**
     * Inserts the specified object as a component in this vector at the
     * specified {@code index}. Each component in this vector with
     * an index greater or equal to the specified {@code index} is
     * shifted upward to have an index one greater than the value it had
     * previously.
     *
     * <p>The index must be a value greater than or equal to {@code 0}
     * and less than or equal to the current size of the vector. (If the
     * index is equal to the current size of the vector, the new element
     * is appended to the Vector.)
     *
     * <p>This method is identical in functionality to the
     * {@link #add(int, Object) add(int, E)}
     * method (which is part of the {@link List} interface).  Note that the
     * {@code add} method reverses the order of the parameters, to more closely
     * match array usage.
     *
     * @param      obj     the component to insert
     * @param      index   where to insert the new component
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index > size()})
     */
    public synchronized void insertElementAt(java_v.lang_v.Integer obj, int index) {
        modCount++;
        if (index > elementCount) {
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();
        }
        ensureCapacityHelper(elementCount + 1);
        java_v.lang_v.System.arraycopyInteger(elementData, index, elementData,
                                              index + 1, elementCount - index);
        elementData[index] = obj;
        elementCount++;
    }

    /**
     * Adds the specified component to the end of this vector,
     * increasing its size by one. The capacity of this vector is
     * increased if its size becomes greater than its capacity.
     *
     * <p>This method is identical in functionality to the
     * {@link #add(Object) add(E)}
     * method (which is part of the {@link List} interface).
     *
     * @param   obj   the component to be added
     */
    public synchronized void addElement(java_v.lang_v.Integer obj) {
        modCount++;
        ensureCapacityHelper(elementCount + 1);
        elementData[elementCount++] = obj;
    }

    /**
     * Removes the first (lowest-indexed) occurrence of the argument
     * from this vector. If the object is found in this vector, each
     * component in the vector with an index greater or equal to the
     * object's index is shifted downward to have an index one smaller
     * than the value it had previously.
     *
     * <p>This method is identical in functionality to the
     * {@link #remove(Object)} method (which is part of the
     * {@link List} interface).
     *
     * @param   obj   the component to be removed
     * @return  {@code true} if the argument was a component of this
     *          vector; {@code false} otherwise.
     */
    public synchronized boolean removeElement(java_v.lang_v.Integer obj) {
        modCount++;
        int i = indexOf0(obj);
        if (i >= 0) {
            removeElementAt(i);
            return true;
        }
        return false;
    }

    /**
     * Returns a clone of this vector. The copy will contain a
     * reference to a clone of the internal data array, not a reference
     * to the original internal data array of this {@code Vector} object.
     *
     * @return  a clone of this vector
     */
    public synchronized Vector cloneMe() {
        try {
            Vector v = (Vector) super.clone();
            v.elementData = Arrays.copyOf(elementData, elementCount);
            v.modCount = 0;
            return v;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError();
        }
    }

    java_v.lang_v.Integer elementData(int index) {
        return elementData[index];
    }

    /**
     * Returns the element at the specified position in this Vector.
     *
     * @param index index of the element to return
     * @return object at the specified index
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *            ({@code index < 0 || index >= size()})
     * @since 1.2
     */
    public synchronized java_v.lang_v.Integer get(int index) {
        if (index >= elementCount)
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();

        return elementData(index);
    }

    /**
     * Replaces the element at the specified position in this Vector with the
     * specified element.
     *
     * @param index index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     * @since 1.2
     */
    public synchronized java_v.lang_v.Integer set(int index,
                                                  java_v.lang_v.Integer element) {
        if (index >= elementCount)
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();

        java_v.lang_v.Integer oldValue = elementData(index);
        elementData[index] = element;
        return oldValue;
    }

    /**
     * Appends the specified element to the end of this Vector.
     *
     * @param e element to be appended to this Vector
     * @return {@code true} (as specified by {@link Collection#add})
     * @since 1.2
     */
    public synchronized boolean add(java_v.lang_v.Integer e) {
        modCount++;
        ensureCapacityHelper(elementCount + 1);
        elementData[elementCount++] = e;
        return true;
    }

    /**
     * Removes the first occurrence of the specified element in this Vector
     * If the Vector does not contain the element, it is unchanged.  More
     * formally, removes the element with the lowest index i such that
     * {@code (o==null ? get(i)==null : o.equals(get(i)))} (if such
     * an element exists).
     *
     * @param o element to be removed from this Vector, if present
     * @return true if the Vector contained the specified element
     * @since 1.2
     */
    public boolean remove(java_v.lang_v.Integer o) {
        return removeElement(o);
    }

    /**
     * Inserts the specified element at the specified position in this Vector.
     * Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     *
     * @param index index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index > size()})
     * @since 1.2
     */
    public void add(int index, java_v.lang_v.Integer element) {
        insertElementAt(element, index);
    }

    /**
     * Removes the element at the specified position in this Vector.
     * Shifts any subsequent elements to the left (subtracts one from their
     * indices).  Returns the element that was removed from the Vector.
     *
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     * @param index the index of the element to be removed
     * @return element that was removed
     * @since 1.2
     */
    public synchronized java_v.lang_v.Integer removeIndex(int index) {
        modCount++;
        if (index >= elementCount)
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();
        java_v.lang_v.Integer oldValue = elementData(index);

        int numMoved = elementCount - index - 1;
        if (numMoved > 0)
            java_v.lang_v.System.arraycopyInteger(elementData, index + 1,
                                                  elementData, index, numMoved);
        elementData[--elementCount] = null; // Let gc do its work

        return oldValue;
    }

    /**
     * Removes all of the elements from this Vector.  The Vector will
     * be empty after this call returns (unless it throws an exception).
     *
     * @since 1.2
     */
    public void clear() {
        removeAllElements();
    }

    // Bulk Operations

    /**
     * Returns true if this Vector contains all of the elements in the
     * specified Collection.
     *
     * @param   c a collection whose elements will be tested for containment
     *          in this Vector
     * @return true if this Vector contains all of the elements in the
     *         specified collection
     * @throws NullPointerException if the specified collection is null
     */
    public synchronized boolean containsAll(Collection c) {
        return super.containsAll(c);
    }

    /**
     * Appends all of the elements in the specified Collection to the end of
     * this Vector, in the order that they are returned by the specified
     * Collection's Iterator.  The behavior of this operation is undefined if
     * the specified Collection is modified while the operation is in progress.
     * (This implies that the behavior of this call is undefined if the
     * specified Collection is this Vector, and this Vector is nonempty.)
     *
     * @param c elements to be inserted into this Vector
     * @return {@code true} if this Vector changed as a result of the call
     * @throws NullPointerException if the specified collection is null
     * @since 1.2
     */
    public synchronized boolean addAll(Collection c) {
        modCount++;
        java_v.lang_v.Integer[] a = c.toArray();
        int numNew = a.length;
        ensureCapacityHelper(elementCount + numNew);
        java_v.lang_v.System.arraycopyInteger(a, 0, elementData, elementCount,
                                              numNew);
        elementCount += numNew;
        return numNew != 0;
    }

    /**
     * Removes from this Vector all of its elements that are contained in the
     * specified Collection.
     *
     * @param c a collection of elements to be removed from the Vector
     * @return true if this Vector changed as a result of the call
     * @throws ClassCastException if the types of one or more elements
     *         in this vector are incompatible with the specified
     *         collection (optional)
     * @throws NullPointerException if this vector contains one or more null
     *         elements and the specified collection does not support null
     *         elements (optional), or if the specified collection is null
     * @since 1.2
     */
    public synchronized boolean removeAll(Collection c) {
        return super.removeAll(c);
    }

    /**
     * Retains only the elements in this Vector that are contained in the
     * specified Collection.  In other words, removes from this Vector all
     * of its elements that are not contained in the specified Collection.
     *
     * @param c a collection of elements to be retained in this Vector
     *          (all other elements are removed)
     * @return true if this Vector changed as a result of the call
     * @throws ClassCastException if the types of one or more elements
     *         in this vector are incompatible with the specified
     *         collection (optional)
     * @throws NullPointerException if this vector contains one or more null
     *         elements and the specified collection does not support null
     *         elements (optional), or if the specified collection is null
     * @since 1.2
     */
    public synchronized boolean retainAll(Collection c) {
        return super.retainAll(c);
    }

    /**
     * Inserts all of the elements in the specified Collection into this
     * Vector at the specified position.  Shifts the element currently at
     * that position (if any) and any subsequent elements to the right
     * (increases their indices).  The new elements will appear in the Vector
     * in the order that they are returned by the specified Collection's
     * iterator.
     *
     * @param index index at which to insert the first element from the
     *              specified collection
     * @param c elements to be inserted into this Vector
     * @return {@code true} if this Vector changed as a result of the call
     * @throws ArrayIndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index > size()})
     * @throws NullPointerException if the specified collection is null
     * @since 1.2
     */
    public synchronized boolean addAll(int index, Collection c) {
        modCount++;
        if (index < 0 || index > elementCount)
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();

        java_v.lang_v.Integer[] a = c.toArray();
        int numNew = a.length;
        ensureCapacityHelper(elementCount + numNew);

        int numMoved = elementCount - index;
        if (numMoved > 0)
            java_v.lang_v.System.arraycopyInteger(elementData, index,
                                                  elementData, index + numNew, numMoved);

        java_v.lang_v.System.arraycopyInteger(a, 0, elementData, index, numNew);
        elementCount += numNew;
        return numNew != 0;
    }

    /**
     * Returns a view of the portion of this List between fromIndex,
     * inclusive, and toIndex, exclusive.  (If fromIndex and toIndex are
     * equal, the returned List is empty.)  The returned List is backed by this
     * List, so changes in the returned List are reflected in this List, and
     * vice-versa.  The returned List supports all of the optional List
     * operations supported by this List.
     *
     * <p>This method eliminates the need for explicit range operations (of
     * the sort that commonly exist for arrays).  Any operation that expects
     * a List can be used as a range operation by operating on a subList view
     * instead of a whole List.  For example, the following idiom
     * removes a range of elements from a List:
     * <pre>
     *      list.subList(from, to).clear();
     * </pre>
     * Similar idioms may be constructed for indexOf and lastIndexOf,
     * and all of the algorithms in the Collections class can be applied to
     * a subList.
     *
     * <p>The semantics of the List returned by this method become undefined if
     * the backing list (i.e., this List) is <i>structurally modified</i> in
     * any way other than via the returned List.  (Structural modifications are
     * those that change the size of the List, or otherwise perturb it in such
     * a fashion that iterations in progress may yield incorrect results.)
     *
     * @param fromIndex low endpoint (inclusive) of the subList
     * @param toIndex high endpoint (exclusive) of the subList
     * @return a view of the specified range within this List
     * @throws IndexOutOfBoundsException if an endpoint index value is out of range
     *         {@code (fromIndex < 0 || toIndex > size)}
     * @throws IllegalArgumentException if the endpoint indices are out of order
     *         {@code (fromIndex > toIndex)}
     */
    public synchronized List subList(int fromIndex, int toIndex) {
        return Collections.synchronizedList(super.subList(fromIndex, toIndex),
                                            this);
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list.
     * The specified index indicates the first element that would be
     * returned by an initial call to {@link ListIterator#next next}.
     * An initial call to {@link ListIterator#previous previous} would
     * return the element with the specified index minus one.
     *
     * <p>The returned list iterator is <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public synchronized ListIterator listIterator(int index) {
        if (index < 0 || index > elementCount)
            throw new java_v.lang_v.IndexOutOfBoundsException();
        return new ListItr(index);
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence).
     *
     * <p>The returned list iterator is <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @see #listIterator(int)
     */
    public synchronized ListIterator listIterator() {
        return new ListItr(0);
    }

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     *
     * <p>The returned iterator is <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    public synchronized Iterator iterator() {
        return new Itr();
    }

    /**
     * The only purpose of this method is allowing 
     * Chicory to access the private fields of this class.
     * 
     */
    private void unreachable_method() {
    }

    /**
     * An optimized version of AbstractList.Itr
     */
    private class Itr implements Iterator {
        int cursor; // index of next element to return
        int lastRet = -1; // index of last element returned; -1 if no such
        int expectedModCount = modCount;

        public boolean hasNext() {
            // Racy but within spec, since modifications are checked
            // within or after synchronization in next/previous
            return cursor != elementCount;
        }

        public java_v.lang_v.Integer next() {
            synchronized (Vector.this) {
                checkForComodification();
                int i = cursor;
                if (i >= elementCount)
                    throw new NoSuchElementException();
                cursor = i + 1;
                return elementData(lastRet = i);
            }
        }

        public void remove() {
            if (lastRet == -1)
                throw new IllegalStateException();
            synchronized (Vector.this) {
                checkForComodification();
                Vector.this.removeIndex(lastRet);
                expectedModCount = modCount;
            }
            cursor = lastRet;
            lastRet = -1;
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }

    /**
     * An optimized version of AbstractList.ListItr
     */
    final class ListItr extends Itr implements ListIterator {
        ListItr(int index) {
            super();
            cursor = index;
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public java_v.lang_v.Integer previous() {
            synchronized (Vector.this) {
                checkForComodification();
                int i = cursor - 1;
                if (i < 0)
                    throw new NoSuchElementException();
                cursor = i;
                return elementData(lastRet = i);
            }
        }

        public void set(java_v.lang_v.Integer e) {
            if (lastRet == -1)
                throw new IllegalStateException();
            synchronized (Vector.this) {
                checkForComodification();
                Vector.this.set(lastRet, e);
            }
        }

        public void add(java_v.lang_v.Integer e) {
            int i = cursor;
            synchronized (Vector.this) {
                checkForComodification();
                Vector.this.add(i, e);
                expectedModCount = modCount;
            }
            cursor = i + 1;
            lastRet = -1;
        }
    }
}
