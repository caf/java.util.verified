/*
 * COPYRIGHT NOTICE FOR JAVA CODE:
 *
 * Copyright 1997-2007 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */
 
 
/*
 * COPYRIGHT NOTICE FOR JML ANNOTATIONS:
 *
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * The JML annotations are free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 */
 


package java_v.util_v;

import java_v.lang_v.Integer;
import java_v.theories.TLists;


public class ArrayDeque extends AbstractCollection {

    // caf: Added to experiment with specification style, but then not really used
    /*@ 
      @ ensures \result.length == count();
      @ ensures head < tail ==> (\forall int i; 0 <= i && i < count() ==> \result[i] == elements[head + i]);
      @ ensures head > tail ==> 
      @     (\forall int i; 0 <= i && i < count() - tail ==> \result[i] == elements[tail + i]) &&
      @     (\forall int i; count() - tail <= i && i < count() 
      @                         ==> \result[i] == elements[i - count() + tail]);
      @ ensures head == tail ==> \result.length == 0;
      @*/
    public/*@ pure @*/java_v.lang_v.Integer[] active() {
        int ic = head, j = 0;
        java_v.lang_v.Integer el = elements[ic];
        int size = count();
        java_v.lang_v.Integer[] Result = new java_v.lang_v.Integer[size];

        /*@ loop_invariant el == elements[ic];
          @
          @ loop_invariant head <= tail ==> head <= ic && ic <= tail;
          @ loop_invariant head <= tail ==> j == ic - head;
          @ loop_invariant head <= tail ==> 
          @              (\forall int k; 0 <= k && k < j ==> Result[k] == elements[head + k]);
          @
          @ loop_invariant head > tail ==> head <= ic && ic < elements.length || 0 <= ic && ic <= tail;
          @ loop_invariant head > tail ==> ( head <= ic && ic < elements.length ==> j == ic - head );
          @ loop_invariant head > tail ==> ( 0 <= ic && ic <= tail ==> j == ic + elements.length - head );
          @ loop_invariant head > tail ==> ( head <= ic && ic < elements.length ==>
          @                      (\forall int k; 0 <= k && k < j ==> Result[k] == elements[head + k]) );
          @ loop_invariant head > tail ==> ( 0 <= ic && ic <= tail ==> 
          @      (\forall int k; elements.length - head <= k && k < elements.length - head + j 
          @                                    ==> Result[k] == elements[k - elements.length + head]) );
          @
          @ loop_invariant (\forall ArrayDeque ad; ad != null ==>
          @   (\forall int k; 0 <= k && k < ad.elements.length ==> ad.elements[k] == \old(ad.elements[k]))); // framing
          @*/
        while (ic != tail) {
            Result[j] = el;
            if (ic == elements.length) {
                ic = 0; // Wrap over
            } else {
                ic++;
            }
            el = elements[ic];
            j++;
        }
        return Result;
    }

    // caf: Added for specification purposes
    /*@ 
      @ ensures 0 <= \result && \result <= elements.length;
      @ ensures head == tail ==> \result == 0;
      @ ensures head < tail ==> \result == tail - head;
      @ ensures head > tail ==> \result == (elements.length - head + tail); 
      @*/
    public/*@ pure @*/int count() {
        return ((head <= tail) ? tail - head : (elements.length - head + tail));
    }

    //@ invariant elements != null;
    //@ invariant 0 <= head && head < elements.length;
    //@ invariant 0 <= tail && tail < elements.length;
    //@ invariant elements[tail] == null;
    //@ invariant elements.owner == this;

    //@ invariant head < tail ==> TLists.neq(elements, head, tail, null);
    //@ invariant head < tail ==> TLists.eq(elements, 0, head, null);
    //@ invariant head < tail ==> TLists.eq(elements, tail, elements.length, null);

    //@ invariant tail < head ==> TLists.neq(elements, 0, tail, null);
    //@ invariant tail < head ==> TLists.neq(elements, head, elements.length, null);
    //@ invariant tail < head ==> TLists.neq(elements, tail, head, null);

    //@ invariant head == tail ==> TLists.eq(elements, 0, elements.length, null);

    /**
     * The array in which the elements of the deque are stored.
     * The capacity of the deque is the length of this array, which is
     * always a power of two. The array is never allowed to become
     * full, except transiently within an addX method where it is
     * resized (see doubleCapacity) immediately upon becoming full,
     * thus avoiding head and tail wrapping around to equal each
     * other.  We also guarantee that all array cells not holding
     * deque elements are always null.
     */
    private java_v.lang_v.Integer[] elements;

    /**
     * The index of the element at the head of the deque (which is the
     * element that would be removed by remove() or pop()); or an
     * arbitrary number equal to tail if the deque is empty.
     */
    private int head;

    /**
     * The index at which the next element would be added to the tail
     * of the deque (via addLast(E), add(E), or push(E)).
     */
    private int tail;

    /**
     * The minimum capacity that we'll use for a newly created deque.
     * Must be a power of 2.
     */
    private static final int MIN_INITIAL_CAPACITY = 8;

    /**
     * Removes the first occurrence of the specified element in this
     * deque (when traversing the deque from head to tail).
     * If the deque does not contain the element, it is unchanged.
     * More formally, removes the first element <tt>e</tt> such that
     * <tt>o.equals(e)</tt> (if such an element exists).
     * Returns <tt>true</tt> if this deque contained the specified element
     * (or equivalently, if this deque changed as a result of the call).
     *
     * @param value element to be removed from this deque, if present
     * @return <tt>true</tt> if the deque contained the specified element
     */
    // Focusing on the part of the specification interesting for the loop.
    /*@ 
      @ assignable head, tail, elements[*];
      @
      @ ensures !\result ==> (value == null || !TLists.deqHas(elements, head, tail, tail, value)) &&
      @               head == \old(head) && tail == \old(tail) && 
      @               (\forall int i; 0 <= i && i < elements.length ==> elements[i] == \old(elements[i]));
      @ ensures \result ==> value != null && \old(TLists.deqHas(elements, head, tail, tail, value));
      @*/
    public boolean removeFirstOccurrence(java_v.lang_v.Integer value)
        throws ConcurrentModificationException {
        if (value == null)
            return false;

        int ic = head;
        java_v.lang_v.Integer x = elements[ic];

        //@ loop_invariant x == elements[ic];
        //@ loop_invariant TLists.deqWithin(elements, head, tail, ic);
        //@ loop_invariant !TLists.deqHas(elements, head, tail, ic, value); 
        //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; v.head == \old(v.head)); // framing
        //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; v.tail== \old(v.tail)); // framing
        //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; (\forall int i ; 0<=i && i< v.elements.length ; v.elements[i]==\old(v.elements[i]))); // framing
        while (x != null) {

            if (value == x) {
                break;
            }
            if (ic == elements.length) {
                ic = 0;
            } else { // caf: the following else branch was strangely missing but it's clearly needed...
                ic++;
            }
            x = elements[ic];
        }
        if (x != null) {
            delete(ic);
            return true;
        } else {
            return false;
        }
    }

    private /*@ pure @*/ void checkInvariants() {
        boolean cond1 = elements[tail] == null;
        if (!cond1)
            throw new RuntimeException();

        boolean cond2 = (head == tail ? elements[head] == null
                         : (elements[head] != null && elements[(tail - 1)
                                                               & (elements.length - 1)] != null));
        if (!cond2)
            throw new RuntimeException();

        boolean cond3 = (elements[(head - 1) & (elements.length - 1)] == null);
        if (!cond3)
            throw new RuntimeException();

    }

    /**
     * Removes the element at the specified position in the elements array,
     * adjusting head and tail as necessary.  This can result in motion of
     * elements backwards or forwards in the array.
     *
     * <p>This method is called delete rather than remove to emphasize
     * that its semantics differ from those of {@link List#removeIndex(int)}.
     *
     * @return true if elements moved backwards
     */
    /*@
      @ requires 0 <= index && index < elements.length;
      @ requires TLists.deqWithin(elements, head, tail, index);
      @
      @ assignable head, tail, elements[*];
      @ 
      @*/
    private boolean delete(int index) throws ConcurrentModificationException {
        checkInvariants();
        final java_v.lang_v.Integer[] elements = this.elements;
        final int mask = elements.length - 1;
        final int h = head;
        final int t = tail;
        final int front = (index - h) & mask;
        final int back = (t - index) & mask;

        // Invariant: head <= i < tail mod circularity
        if (front >= ((t - h) & mask))
            throw new ConcurrentModificationException();

        // Optimize for least element motion
        if (front < back) {
            if (h <= index) {
                java_v.lang_v.System.arraycopyInteger(elements, h, elements,
                                                      h + 1, front);
            } else { // Wrap around
                java_v.lang_v.System.arraycopyInteger(elements, 0, elements, 1,
                                                      index);
                elements[0] = elements[mask];
                java_v.lang_v.System.arraycopyInteger(elements, h, elements,
                                                      h + 1, mask - h);
            }
            elements[h] = null;
            head = (h + 1) & mask;
            return false;
        } else {
            if (index < t) { // Copy the null tail as well
                java_v.lang_v.System.arraycopyInteger(elements, index + 1,
                                                      elements, index, back);
                tail = t - 1;
            } else { // Wrap around
                java_v.lang_v.System.arraycopyInteger(elements, index + 1,
                                                      elements, index, mask - index);
                elements[mask] = elements[0];
                java_v.lang_v.System
                    .arraycopyInteger(elements, 1, elements, 0, t);
                tail = (t - 1) & mask;
            }
            return true;
        }
    }

    /*@ also 
      @ ensures !\result ==> (value == null || !TLists.deqHas(elements, head, tail, tail, value));
      @ ensures \result ==> value != null && TLists.deqHas(elements, head, tail, tail, value);
      @*/
    public/*@ pure @*/boolean contains(java_v.lang_v.Integer value) {
        if (value == null)
            return false;

        int ic = head;
        java_v.lang_v.Integer x = elements[ic];

        //@ loop_invariant x == elements[ic];
        //@ loop_invariant TLists.deqWithin(elements, head, tail, ic);
        //@ loop_invariant !TLists.deqHas(elements, head, tail, ic, value);
        while (x != null) {
            if (value == x) {
                break;
            }
            if (ic == elements.length) {
                ic = 0;
            } else { // caf: the following else branch was strangely missing but it's clearly needed...
                ic++;
            }
            x = elements[ic];
        }
        if (x != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes all of the elements from this deque.
     * The deque will be empty after this call returns.
     */
    /*@ also
      @ assignable head, tail, elements[*];
      @
      @ ensures TLists.eq(elements, 0, elements.length, null);
      // Equivalent to the previous one, for uniformity of presentation
      @ ensures \old(head) != \old(tail) ==> TLists.deqEq(elements, 0, elements.length, elements.length, null);
      @ ensures head == tail;
      @*/
    public void clear() {
        int h = head;
        int t = tail;
        if (h != t) { // clear all cells
            head = tail = 0;
            int ic = h;

            //@ loop_invariant h == \old(head);   // Fail to establish this even with assumes
            //@ loop_invariant t == \old(tail);   // Fail to establish this even with assumes
            //@ loop_invariant TLists.deqWithin(elements, h, t, ic); 
            //@ loop_invariant TLists.deqEq(elements, h, t, ic, null); 
            //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; v.head == \old(v.head)); // framing
            //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; v.tail== \old(v.tail)); // framing
            //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; (\forall int i ; 0<=i && i< v.elements.length ; v.elements[i]==\old(v.elements[i]))); // framing
            while (ic != t) {
                elements[ic] = null;
                ic = (ic + 1);
                if (ic == elements.length)
                    ic = 0;
            }
        }
    }

    /**
     * Removes the last occurrence of the specified element in this
     * deque (when traversing the deque from head to tail).
     * If the deque does not contain the element, it is unchanged.
     * More formally, removes the last element <tt>e</tt> such that
     * <tt>o.equals(e)</tt> (if such an element exists).
     * Returns <tt>true</tt> if this deque contained the specified element
     * (or equivalently, if this deque changed as a result of the call).
     *
     * @param value element to be removed from this deque, if present
     * @return <tt>true</tt> if the deque contained the specified element
     * @throws ConcurrentModificationException 
     */

    // Focusing on the part of the specification interesting for the loop.
    /*@ 
      @ assignable head, tail, elements[*];
      @
      @ ensures !\result ==> (value == null || !TLists.deqHas(elements, head, tail, tail, value)) &&
      @               head == \old(head) && tail == \old(tail) && 
      @               (\forall int i; 0 <= i && i < elements.length ==> elements[i] == \old(elements[i]));
      @ ensures \result ==> value != null && 
      @                     \old(TLists.deqHas(elements, head, tail, tail, value));
      @*/
    public boolean removeLastOccurrence(java_v.lang_v.Integer value)
        throws ConcurrentModificationException {
        if (value == null)
            return false;

        int indexToRemove = -1;

        if (head < tail) {

            int ic = tail - 1;

            //@ loop_invariant head - 1 <= ic && ic < tail;
            //@ loop_invariant TLists.neq(elements, ic + 1, tail, value);
            //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; v.head == \old(v.head)); // framing
            //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; v.tail== \old(v.tail)); // framing
            //@ loop_invariant (\forall ArrayDeque v ; v!=null && v!=this; (\forall int i ; 0<=i && i< v.elements.length ; v.elements[i]==\old(v.elements[i]))); // framing
            for (; ic >= head; ic--) {

                if (value == elements[ic]) {
                    break;
                }
            }
            if (ic >= head) {
                indexToRemove = ic;
            }

        } else {
            int ic = tail - 1;

            //@ loop_invariant -1 <= ic && ic < tail;
            //@ loop_invariant TLists.neq(elements, ic + 1, tail, value);
            for (; ic >= 0; ic--) {
                if (value == elements[ic])
                    break;
            }

            if (ic >= 0) {
                indexToRemove = ic;
            } else {

                ic = elements.length - 1;

                //@ loop_invariant head - 1 <= ic && ic < elements.length;
                //@ loop_invariant TLists.neq(elements, 0, tail, value);
                //@ loop_invariant TLists.neq(elements, ic + 1, elements.length, value);
                for (; ic >= head; ic--) {
                    if (value == elements[ic])
                        break;

                }
                if (ic >= head) {
                    indexToRemove = ic;
                }

            }

        }

        if (indexToRemove != -1) {
            // A little nudge.
            //@ assert \old(TLists.deqHas(elements, head, tail, tail, value));
            delete(indexToRemove);
            return true;
        } else
            return false;
    }

    /**
     * Allocate empty array to hold the given number of elements.
     *
     * @param numElements  the number of elements to hold
     */
    private void allocateElements(int numElements) {
        int initialCapacity = MIN_INITIAL_CAPACITY;
        // Find the best power of two to hold elements.
        // Tests "<=" because arrays aren't kept full.
        if (numElements >= initialCapacity) {
            initialCapacity = numElements;
            initialCapacity |= MathAux.unsigned_right_shift(initialCapacity, 1);
            initialCapacity |= MathAux.unsigned_right_shift(initialCapacity, 2);
            initialCapacity |= MathAux.unsigned_right_shift(initialCapacity, 4);
            initialCapacity |= MathAux.unsigned_right_shift(initialCapacity, 8);
            initialCapacity |= MathAux
                .unsigned_right_shift(initialCapacity, 16);
            initialCapacity++;

            if (initialCapacity < 0) // Too many elements, must back off
                initialCapacity = MathAux.unsigned_right_shift(initialCapacity,
                                                               1);// Good luck allocating 2 ^ 30 elements
        }
        elements = new java_v.lang_v.Integer[initialCapacity];
    }

    /**
     * Double the capacity of this deque.  Call only when full, i.e.,
     * when head and tail have wrapped around to become equal.
     */
    private void doubleCapacity() {
        int p = head;
        int n = elements.length;
        int r = n - p; // number of elements to the right of p
        int newCapacity = n << 1;
        if (newCapacity < 0)
            throw new IllegalStateException("Sorry, deque too big");
        java_v.lang_v.Integer[] a = new java_v.lang_v.Integer[newCapacity];
        java_v.lang_v.System.arraycopyInteger(elements, p, a, 0, r);
        java_v.lang_v.System.arraycopyInteger(elements, 0, a, r, p);
        elements = a;
        head = 0;
        tail = n;
    }

    /**
     * Copies the elements from our element array into the specified array,
     * in order (from first to last element in the deque).  It is assumed
     * that the array is large enough to hold all elements in the deque.
     *
     * @return its argument
     */
    private java_v.lang_v.Integer[] copyElements(java_v.lang_v.Integer[] a) {
        if (head < tail) {
            java_v.lang_v.System.arraycopyInteger(elements, head, a, 0, size());
        } else if (head > tail) {
            int headPortionLen = elements.length - head;
            java_v.lang_v.System.arraycopyInteger(elements, head, a, 0,
                                                  headPortionLen);
            java_v.lang_v.System.arraycopyInteger(elements, 0, a, headPortionLen,
                                                  tail);
        }
        return a;
    }

    /**
     * Constructs an empty array deque with an initial capacity
     * sufficient to hold 16 elements.
     */
    public ArrayDeque() {
        elements = new java_v.lang_v.Integer[16];
    }

    /**
     * Constructs an empty array deque with an initial capacity
     * sufficient to hold the specified number of elements.
     *
     * @param numElements  lower bound on initial capacity of the deque
     */
    public ArrayDeque(int numElements) {
        allocateElements(numElements);
    }

    /**
     * Constructs a deque containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.  (The first element returned by the collection's
     * iterator becomes the first element, or <i>front</i> of the
     * deque.)
     *
     * @param c the collection whose elements are to be placed into the deque
     * @throws NullPointerException if the specified collection is null
     */
    public ArrayDeque(Collection c) {
        allocateElements(c.size());
        addAll(c);
    }

    /**
     * Inserts the specified element at the front of this deque.
     *
     * @param e the element to add
     * @throws NullPointerException if the specified element is null
     */
    public void addFirst(java_v.lang_v.Integer e) {
        if (e == null)
            throw new NullPointerException();
        elements[head = (head - 1) & (elements.length - 1)] = e;
        if (head == tail)
            doubleCapacity();
    }

    /**
     * Inserts the specified element at the end of this deque.
     *
     * <p>This method is equivalent to {@link #add}.
     *
     * @param e the element to add
     * @throws NullPointerException if the specified element is null
     */
    public void addLast(java_v.lang_v.Integer e) {
        if (e == null)
            throw new NullPointerException();
        elements[tail] = e;
        if ((tail = (tail + 1) & (elements.length - 1)) == head)
            doubleCapacity();
    }

    /**
     * Inserts the specified element at the front of this deque.
     *
     * @param e the element to add
     * @return <tt>true</tt> (as specified by {@link Deque#offerFirst})
     * @throws NullPointerException if the specified element is null
     */
    public boolean offerFirst(java_v.lang_v.Integer e) {
        addFirst(e);
        return true;
    }

    /**
     * Inserts the specified element at the end of this deque.
     *
     * @param e the element to add
     * @return <tt>true</tt> (as specified by {@link Deque#offerLast})
     * @throws NullPointerException if the specified element is null
     */
    public boolean offerLast(java_v.lang_v.Integer e) {
        addLast(e);
        return true;
    }

    /**
     * @throws NoSuchElementException {@inheritDoc}
     */
    public java_v.lang_v.Integer removeFirst() {
        java_v.lang_v.Integer x = pollFirst();
        if (x == null)
            throw new NoSuchElementException();
        return x;
    }

    /**
     * @throws NoSuchElementException {@inheritDoc}
     */
    public java_v.lang_v.Integer removeLast() {
        java_v.lang_v.Integer x = pollLast();
        if (x == null)
            throw new NoSuchElementException();
        return x;
    }

    public java_v.lang_v.Integer pollFirst() {
        int h = head;
        java_v.lang_v.Integer result = elements[h]; // Element is null if deque empty
        if (result == null)
            return null;
        elements[h] = null; // Must null out slot
        head = (h + 1) & (elements.length - 1);
        return result;
    }

    public java_v.lang_v.Integer pollLast() {
        int t = (tail - 1) & (elements.length - 1);
        java_v.lang_v.Integer result = elements[t];
        if (result == null)
            return null;
        elements[t] = null;
        tail = t;
        return result;
    }

    /**
     * @throws NoSuchElementException {@inheritDoc}
     */
    public java_v.lang_v.Integer getFirst() {
        java_v.lang_v.Integer x = elements[head];
        if (x == null)
            throw new NoSuchElementException();
        return x;
    }

    /**
     * @throws NoSuchElementException {@inheritDoc}
     */
    public java_v.lang_v.Integer getLast() {
        java_v.lang_v.Integer x = elements[(tail - 1) & (elements.length - 1)];
        if (x == null)
            throw new NoSuchElementException();
        return x;
    }

    public java_v.lang_v.Integer peekFirst() {
        return elements[head]; // elements[head] is null if deque empty
    }

    public java_v.lang_v.Integer peekLast() {
        return elements[(tail - 1) & (elements.length - 1)];
    }

    /**
     * Inserts the specified element at the end of this deque.
     *
     * <p>This method is equivalent to {@link #addLast}.
     *
     * @param e the element to add
     * @return <tt>true</tt> (as specified by {@link Collection#add})
     * @throws NullPointerException if the specified element is null
     */
    public boolean add(java_v.lang_v.Integer e) {
        addLast(e);
        return true;
    }

    /**
     * Inserts the specified element at the end of this deque.
     *
     * <p>This method is equivalent to {@link #offerLast}.
     *
     * @param e the element to add
     * @return <tt>true</tt> (as specified by {@link Queue#offer})
     * @throws NullPointerException if the specified element is null
     */
    public boolean offer(java_v.lang_v.Integer e) {
        return offerLast(e);
    }

    /**
     * Retrieves and removes the head of the queue represented by this deque.
     *
     * This method differs from {@link #poll poll} only in that it throws an
     * exception if this deque is empty.
     *
     * <p>This method is equivalent to {@link #removeFirst}.
     *
     * @return the head of the queue represented by this deque
     * @throws NoSuchElementException {@inheritDoc}
     */
    public java_v.lang_v.Integer remove() {
        return removeFirst();
    }

    /**
     * Retrieves and removes the head of the queue represented by this deque
     * (in other words, the first element of this deque), or returns
     * <tt>null</tt> if this deque is empty.
     *
     * <p>This method is equivalent to {@link #pollFirst}.
     *
     * @return the head of the queue represented by this deque, or
     *         <tt>null</tt> if this deque is empty
     */
    public java_v.lang_v.Integer poll() {
        return pollFirst();
    }

    /**
     * Retrieves, but does not remove, the head of the queue represented by
     * this deque.  This method differs from {@link #peek peek} only in
     * that it throws an exception if this deque is empty.
     *
     * <p>This method is equivalent to {@link #getFirst}.
     *
     * @return the head of the queue represented by this deque
     * @throws NoSuchElementException {@inheritDoc}
     */
    public java_v.lang_v.Integer element() {
        return getFirst();
    }

    /**
     * Retrieves, but does not remove, the head of the queue represented by
     * this deque, or returns <tt>null</tt> if this deque is empty.
     *
     * <p>This method is equivalent to {@link #peekFirst}.
     *
     * @return the head of the queue represented by this deque, or
     *         <tt>null</tt> if this deque is empty
     */
    public java_v.lang_v.Integer peek() {
        return peekFirst();
    }

    /**
     * Pushes an element onto the stack represented by this deque.  In other
     * words, inserts the element at the front of this deque.
     *
     * <p>This method is equivalent to {@link #addFirst}.
     *
     * @param e the element to push
     * @throws NullPointerException if the specified element is null
     */
    public void push(java_v.lang_v.Integer e) {
        addFirst(e);
    }

    /**
     * Pops an element from the stack represented by this deque.  In other
     * words, removes and returns the first element of this deque.
     *
     * <p>This method is equivalent to {@link #removeFirst()}.
     *
     * @return the element at the front of this deque (which is the top
     *         of the stack represented by this deque)
     * @throws NoSuchElementException {@inheritDoc}
     */
    public java_v.lang_v.Integer pop() {
        return removeFirst();
    }

    /**
     * Returns the number of elements in this deque.
     *
     * @return the number of elements in this deque
     */
    public int size() {
        return (tail - head) & (elements.length - 1);
    }

    /**
     * Returns <tt>true</tt> if this deque contains no elements.
     *
     * @return <tt>true</tt> if this deque contains no elements
     */
    public boolean isEmpty() {
        return head == tail;
    }

    /**
     * Removes a single instance of the specified element from this deque.
     * If the deque does not contain the element, it is unchanged.
     * More formally, removes the first element <tt>e</tt> such that
     * <tt>o.equals(e)</tt> (if such an element exists).
     * Returns <tt>true</tt> if this deque contained the specified element
     * (or equivalently, if this deque changed as a result of the call).
     *
     * <p>This method is equivalent to {@link #removeFirstOccurrence}.
     *
     * @param o element to be removed from this deque, if present
     * @return <tt>true</tt> if this deque contained the specified element
     */
    public boolean remove(java_v.lang_v.Integer o) {
        return removeFirstOccurrence(o);
    }

    /**
     * Returns an array containing all of the elements in this deque
     * in proper sequence (from first to last element).
     *
     * <p>The returned array will be "safe" in that no references to it are
     * maintained by this deque.  (In other words, this method must allocate
     * a new array).  The caller is thus free to modify the returned array.
     *
     * <p>This method acts as bridge between array-based and collection-based
     * APIs.
     *
     * @return an array containing all of the elements in this deque
     */
    public java_v.lang_v.Integer[] toArray() {
        return copyElements(new java_v.lang_v.Integer[size()]);
    }

    /**
     * Returns a copy of this deque.
     *
     * @return a copy of this deque
     */
    public ArrayDeque cloneMe() {
        try {
            ArrayDeque result = (ArrayDeque) super.clone();
            result.elements = Arrays.copyOf(elements, elements.length);
            return result;

        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    /**
     * Serialize this deque.
     *
     * @serialData The current size (<tt>int</tt>) of the deque,
     * followed by all of its elements (each an object reference) in
     * first-to-last order.
     *
     */
    /*
      private void writeObject(ObjectOutputStream s) throws IOException {
      s.defaultWriteObject();

      // Write out size
      s.writeInt(size());

      // Write out elements in order.
      int mask = elements.length - 1;
      for (int i = head; i != tail; i = (i + 1) & mask)
      s.writeObject(elements[i]);
      }
    */

    /**
     * Deserialize this deque.
     */
    /*
      private void readObject(ObjectInputStream s) throws IOException,
      ClassNotFoundException {
      s.defaultReadObject();

      // Read in size and allocate array
      int size = s.readInt();
      allocateElements(size);
      head = 0;
      tail = size;

      // Read in all elements in the proper order.
      for (int i = 0; i < size; i++)
      elements[i] = (java_v.lang_v.Integer) s.readObject();
      }
    */

    /**
     * Returns an iterator over the elements in this deque.  The elements
     * will be ordered from first (head) to last (tail).  This is the same
     * order that elements would be dequeued (via successive calls to
     * {@link #remove} or popped (via successive calls to {@link #pop}).
     *
     * @return an iterator over the elements in this deque
     */
    public Iterator iterator() {
        return new DeqIterator();
    }

    public Iterator descendingIterator() {
        return new DescendingIterator();
    }

    private class DeqIterator implements Iterator {
        /**
         * Index of element to be returned by subsequent call to next.
         */
        private int cursor = head;

        /**
         * Tail recorded at construction (also in remove), to stop
         * iterator and also to check for comodification.
         */
        private int fence = tail;

        /**
         * Index of element returned by most recent call to next.
         * Reset to -1 if element is deleted by a call to remove.
         */
        private int lastRet = -1;

        public boolean hasNext() {
            return cursor != fence;
        }

        public java_v.lang_v.Integer next() {
            if (cursor == fence)
                throw new NoSuchElementException();
            java_v.lang_v.Integer result = elements[cursor];
            // This check doesn't catch all possible comodifications,
            // but does catch the ones that corrupt traversal
            if (tail != fence || result == null)
                throw new ConcurrentModificationException();
            lastRet = cursor;
            cursor = (cursor + 1) & (elements.length - 1);
            return result;
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            if (delete(lastRet)) { // if left-shifted, undo increment in next()
                cursor = (cursor - 1) & (elements.length - 1);
                fence = tail;
            }
            lastRet = -1;
        }
    }

    private class DescendingIterator implements Iterator {
        /*
         * This class is nearly a mirror-image of DeqIterator, using
         * tail instead of head for initial cursor, and head instead of
         * tail for fence.
         */
        private int cursor = tail;
        private int fence = head;
        private int lastRet = -1;

        public boolean hasNext() {
            return cursor != fence;
        }

        public java_v.lang_v.Integer next() {
            if (cursor == fence)
                throw new NoSuchElementException();
            cursor = (cursor - 1) & (elements.length - 1);
            java_v.lang_v.Integer result = elements[cursor];
            if (head != fence || result == null)
                throw new ConcurrentModificationException();
            lastRet = cursor;
            return result;
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            if (!delete(lastRet)) {
                cursor = (cursor + 1) & (elements.length - 1);
                fence = head;
            }
            lastRet = -1;
        }
    }

}
