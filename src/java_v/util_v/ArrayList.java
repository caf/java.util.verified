/*
 * COPYRIGHT NOTICE FOR JAVA CODE:
 *
 * Copyright 1997-2007 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */
 
 
/*
 * COPYRIGHT NOTICE FOR JML ANNOTATIONS:
 *
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * The JML annotations are free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 */
 


package java_v.util_v;

import java_v.theories.TLists;
import java_v.lang_v.Integer;


public class ArrayList extends AbstractList {

    private /*@ spec_public @*/ int modCount;

    //@ invariant elementData != null;
    //@ invariant (\forall int i; size <= i && i < elementData.length ==> elementData[i] == null);
    //@ invariant elementData.owner == this;
    private /*@ spec_public @*/ java_v.lang_v.Integer[] elementData;

    //@ invariant 0 <= size && size <= elementData.length;
    private /*@ spec_public @*/ int size;

    /**
     * Constructs an empty list with the specified initial capacity.
     *
     * @param   initialCapacity   the initial capacity of the list
     * @exception IllegalArgumentException if the specified initial capacity
     *            is negative
     */
    public ArrayList(int initialCapacity) {
        super();
        if (initialCapacity < 0)
            throw new java_v.lang_v.IllegalArgumentException();
        this.elementData = new java_v.lang_v.Integer[initialCapacity];
    }

    /**
     * Constructs an empty list with an initial capacity of ten.
     */
    public ArrayList() {
        this(10);
    }

    /**
     * Constructs a list containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.
     *
     * @param c the collection whose elements are to be placed into this list
     * @throws NullPointerException if the specified collection is null
     */
    public ArrayList(Collection c) {
        elementData = c.toArray();
        size = elementData.length;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the lowest index <tt>i</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     */
    /*@ 
      @ ensures \result == -1 <==> !TLists.in(elementData, 0, size, value);
      @ ensures \result != -1 ==> 0 <= \result && \result < size;
      @ ensures \result != -1 ==> elementData[\result] == value && !TLists.in(elementData, 0, \result, value);
      @*/
    public /*@ pure @*/ int indexOf(java_v.lang_v.Integer value) {
        if (value == null) {
            int ic = 0;
            //@ loop_invariant 0 <= ic && ic <= size;
            //@ loop_invariant !TLists.in(elementData, 0, ic, value);
            for (; ic < size; ic++) {
                if (elementData[ic] == null) {
                    return ic;
                }
            }
        } else {
            int ic = 0;
            //@ loop_invariant 0 <= ic && ic <= size;
            //@ loop_invariant !TLists.in(elementData, 0, ic, value);
            for (; ic < size; ic++) {
                if (value == elementData[ic]) {
                    return ic;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the index of the last occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the highest index <tt>i</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     */
    /*@ also
      @ ensures \result == -1 <==> !TLists.in(elementData, 0, size, value);
      @ ensures \result != -1 ==> 0 <= \result && \result < size;
      @ ensures \result != -1 ==> 
      @          elementData[\result] == value && !TLists.in(elementData, \result + 1, size, value);
      @*/
    public /*@ pure @*/ int lastIndexOf(java_v.lang_v.Integer value) {
        if (value == null) {
            int ic = size - 1;
            //@ loop_invariant -1 <= ic && ic < size;
            //@ loop_invariant !TLists.in(elementData, ic + 1, size, value);
            for (; ic >= 0; ic--) {
                if (elementData[ic] == null) {
                    return ic;
                }
            }
        } else {
            int ic = size - 1;
            //@ loop_invariant -1 <= ic && ic < size;
            //@ loop_invariant !TLists.in(elementData, ic + 1, size, value);
            for (; ic >= 0; ic--) {
                if (value == elementData[ic]) {
                    return ic;
                }
            }
        }
        return -1;
    }

    /*
     * Private remove method that skips bounds checking and does not
     * return the value removed.
     */
    /*@ 
      @ requires 0 <= index && index < size;
      @
      @ assignable modCount, size, elementData[*];
      @
      @ ensures size == \old(size) - 1;
      @ ensures (\forall int i; 0 <= i && i < index ==> elementData[i] == \old(elementData[i]));
      @ ensures (\forall int i; index <= i && i < size ==> elementData[i] == \old(elementData[i + 1]));
      @*/
    private void fastRemove(int index) {
        modCount++;
        int numMoved = size - index - 1;
        if (numMoved > 0)
            java_v.lang_v.System.arraycopyInteger(elementData, index + 1,
                                                  elementData, index, numMoved);
        elementData[--size] = null; // Let gc do its work
    }

    /**
     * Removes the first occurrence of the specified element from this list,
     * if it is present.  If the list does not contain the element, it is
     * unchanged.  More formally, removes the element with the lowest index
     * <tt>i</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>
     * (if such an element exists).  Returns <tt>true</tt> if this list
     * contained the specified element (or equivalently, if this list
     * changed as a result of the call).
     *
     * @param value element to be removed from this list, if present
     * @return <tt>true</tt> if this list contained the specified element
     */
    // Called "remove" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ assignable modCount, size, elementData[*];
      @
      @ ensures !\result ==> size == \old(size);
      @ ensures !\result ==> \old(!TLists.in(elementData, 0, size, value));
      @ ensures !\result ==> (\forall int i; 0 <= i && i < size ==> elementData[i] == \old(elementData[i]));
      @ ensures \result ==> size == \old(size) - 1;
      @ ensures \result ==> \old(TLists.in(elementData, 0, size, value));
      // Cannot prove this even with identical assert.
      // @ ensures \result ==> (\exists int index; 0 <= index && index < \old(size) && 
      // @     (\forall int i; 0 <= i && i < index ==> elementData[i] == \old(elementData[i])) &&
      // @      (\forall int i; index <= i && i < size ==> elementData[i] == \old(elementData[i + 1])));
      @*/
    private boolean remove0(java_v.lang_v.Integer value) {
        int index = 0;
        if (value == null) {
            //@ loop_invariant 0 <= index && index <= size;
            //@ loop_invariant !TLists.in(elementData, 0, index, value);
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; v.modCount == \old(v.modCount)); // framing
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; v.size == \old(v.size)); // framing
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; (\forall int i ; 0 <= i && i < v.elementData.length; v.elementData[i] == \old(v.elementData[i]))); // framing
            for (; index < size; index++) {
                if (elementData[index] == null) {
                    break;
                }
            }
        } else {

            //@ loop_invariant 0 <= index && index <= size;
            //@ loop_invariant !TLists.in(elementData, 0, index, value);
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; v.modCount == \old(v.modCount)); // framing
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; v.size == \old(v.size)); // framing
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; (\forall int i ; 0 <= i && i < v.elementData.length; v.elementData[i] == \old(v.elementData[i]))); // framing
            for (; index < size; index++) {
                if (value == elementData[index]) {
                    break;
                }
            }
        }
        if (index < size) {
            fastRemove(index);
            return true;
        } else {
            return false;
        }
    }

    public boolean remove(java_v.lang_v.Integer value) {
        return remove0(value);
    }

    /**
     * Removes all of the elements from this list.  The list will
     * be empty after this call returns.
     */
    /*@ also
      @ assignable size, modCount, elementData[*];
      @
      @ ensures size == 0;
      @ ensures TLists.eq(elementData, 0, \old(size), null); 
      @ ensures TLists.eq(elementData, \old(size), elementData.length, null);
      @*/
    public void clear() {
        modCount++;

        // Let gc do its work

        int ic = 0;
        //@ loop_invariant 0 <= ic && ic <= \old(size);
        //@ loop_invariant TLists.eq(elementData, 0, ic, null);
        //@ loop_invariant TLists.eq(elementData, \old(size), elementData.length, null);
        // Needed to ensure object invariant holds:
        //@ loop_invariant (\forall ArrayList al; al != null && al != this ==> (\forall int j; 0 <= j && j < al.elementData.length ==> al.elementData[j] == \old(al.elementData[j]))); 
        //@ loop_invariant (\forall ArrayList al; al != null && al != this; (\forall int j; 0 <= j && j < al.elementData.length ; al.elementData[j] == \old(al.elementData[j]))); // framing
        //@ loop_invariant (\forall ArrayList al; al != null && al != this; al.size == \old(al.size)); // framing
        //@ loop_invariant (\forall ArrayList al; al != null && al != this; al.modCount == \old(al.modCount)); // framing
        for (; ic < size; ic++) {
            elementData[ic] = null;
        }

        size = 0;
    }

    /*@ 
      @ requires c != null;
      @ requires c != this;
      @
      @ assignable size, modCount, elementData[*];
      @
      // Equivalent formulation:
      @ ensures (\forall int i; 0 <= i && i < this.size ==> (c.contains(elementData[i]) == complement));
      @*/
    private boolean batchRemove(Collection c, boolean complement) {
        int r = 0, w = 0;
        boolean modified = false;

        try {

            //@ loop_invariant 0 <= w && w <= r && r <= size;
            //@ loop_invariant (\forall int i; size <= i && i < elementData.length ==> elementData[i] == \old(elementData[i])); 
            //@ loop_invariant (\forall ArrayList al; al != null && al != this ==> (\forall int j; 0 <= j && j < al.elementData.length ==> al.elementData[j] == \old(al.elementData[j]))); 
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; v.modCount == \old(v.modCount)); // framing
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; v.size == \old(v.size)); // framing
            //@ loop_invariant (\forall ArrayList v; v != null && v != this; (\forall int i ; 0 <= i && i < v.elementData.length; v.elementData[i] == \old(v.elementData[i]))); // framing
            for (; r < size; r++) {
                if (c.contains(elementData[r]) == complement) {
                    elementData[w] = elementData[r];
                    w++;
                }
            }

        } finally {
            // Preserve behavioral compatibility with AbstractCollection,
            // even if c.contains() throws.
            if (r != size) {
                java_v.lang_v.System.arraycopyInteger(elementData, r,
                                                      elementData, w, size - r);
                w += size - r;
            }
            if (w != size) {
                int ic = w;

                //@ loop_invariant w <= ic && ic <= size;
                //@ loop_invariant TLists.eq(elementData, w, ic, null);
                //@ loop_invariant TLists.eq(elementData, size, elementData.length, null);
                //@ loop_invariant (\forall ArrayList al; al != null && al != this ==> (\forall int j; 0 <= j && j < al.elementData.length ==> al.elementData[j] == \old(al.elementData[j]))); 
                //@ loop_invariant (\forall ArrayList v ; v!=null && v!=this; v.modCount == \old(v.modCount)); // framing
                //@ loop_invariant (\forall ArrayList v ; v!=null && v!=this; v.size == \old(v.size)); // framing
                //@ loop_invariant (\forall ArrayList v ; v!=null && v!=this; (\forall int i ; 0<=i && i< v.elementData.length ; v.elementData[i]==\old(v.elementData[i]))); // framing
                for (; ic < size;) {
                    elementData[ic] = null;
                    ic++;
                }
                modCount += size - w;
                size = w;
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Returns <tt>true</tt> if this list contains the specified element.
     * More formally, returns <tt>true</tt> if and only if this list contains
     * at least one element <tt>e</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param value element whose presence in this list is to be tested
     * @return <tt>true</tt> if this list contains the specified element
     */
    /*@ also
      @ ensures \result <==> TLists.in(elementData, 0, size, value); 
      @*/
    public/*@ pure @*/boolean contains(java_v.lang_v.Integer value) {
        return indexOf(value) >= 0;
    }

    /*@ also
      @ assignable elementData[*];
      @
      @ ensures (\forall int i; 0 <= i && i < elementData.length ==> elementData[i] == \old(elementData[i]));
      @ ensures \result != null;
      @ ensures \result.length == size;
      @ ensures (\forall int i; 0 <= i && i < \result.length ==> \result[i] == elementData[i]); 
      @ ensures \result.owner == null;
      @*/
    public java_v.lang_v.Integer[] toArray() {
        java_v.lang_v.Integer[] ret_val = Arrays.copyOf(elementData, size);
        return ret_val;
    }

    /*@ also
      @ ensures \result == size;
      @*/
    public/*@ pure @*/int size() {
        return size;
    }

    /*@ normal_behavior
      @ requires 0 <= index && index < size;
      @ ensures \result == elementData[index];
      @
      @ also exceptional_behavior
      @ requires index < 0 || index >= size;
      @ signals (java_v.lang_v.IndexOutOfBoundsException ex) index < 0 || index >= size;
      @*/
    public/*@pure@*/java_v.lang_v.Integer getIndex(int index)
        throws java_v.lang_v.IndexOutOfBoundsException {
        if (index < 0 || index >= size) {
            throw new java_v.lang_v.IndexOutOfBoundsException();
        }

        java_v.lang_v.Integer ret_value = elementData[index];
        return ret_value;
    }

    /**
     * Returns <tt>true</tt> if this list contains no elements.
     *
     * @return <tt>true</tt> if this list contains no elements
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns a shallow copy of this <tt>ArrayList</tt> instance.  (The
     * elements themselves are not copied.)
     *
     * @return a clone of this <tt>ArrayList</tt> instance
     */
    public ArrayList cloneMe() {
        try {
            ArrayList v = (ArrayList) super.clone();
            v.elementData = Arrays.copyOf(elementData, size);
            v.modCount = 0;
            return v;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError();
        }
    }

    /*@ normal_behavior
      @ requires 0 <= index && index < size;
      @ assignable elementData[index];
      @ ensures elementData[index] == new_value;
      @ ensures (\forall int i; 0 <= i && i < size && i != index ==> elementData[i] == \old(elementData[i]));
      @ ensures \result == \old(elementData[index]);
      @
      @ also exceptional_behavior
      @ requires index < 0 || index >= size;
      @ assignable \nothing;
      @ signals (java_v.lang_v.IndexOutOfBoundsException ex) index < 0 || index >= size;
      @*/
    public java_v.lang_v.Integer setIndex(int index, java_v.lang_v.Integer new_value)
        throws java_v.lang_v.IndexOutOfBoundsException {
        if (index < 0 || index >= size) {
            throw new java_v.lang_v.IndexOutOfBoundsException();
        }

        java_v.lang_v.Integer old_value = elementData[index];
        elementData[index] = new_value;
        return old_value;

    }

    /**
     * Trims the capacity of this <tt>ArrayList</tt> instance to be the
     * list's current size.  An application can use this operation to minimize
     * the storage of an <tt>ArrayList</tt> instance.
     */
    public void trimToSize() {
        modCount++;
        int oldCapacity = elementData.length;
        if (size < oldCapacity) {
            elementData = Arrays.copyOf(elementData, size);
        }
    }

    /**
     * Increases the capacity of this <tt>ArrayList</tt> instance, if
     * necessary, to ensure that it can hold at least the number of elements
     * specified by the minimum capacity argument.
     *
     * @param   minCapacity   the desired minimum capacity
     */
    public void ensureCapacity(int minCapacity) {
        modCount++;
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            java_v.lang_v.Integer oldData[] = elementData;
            int newCapacity = (oldCapacity * 3) / 2 + 1;
            if (newCapacity < minCapacity)
                newCapacity = minCapacity;
            // minCapacity is usually close to size, so this is a win:
            elementData = Arrays.copyOf(elementData, newCapacity);
        }
    }

    java_v.lang_v.Integer elementData(int index) {
        return elementData[index];
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param  index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public java_v.lang_v.Integer get(int index) {
        rangeCheck(index);

        return elementData(index);
    }

    /**
     * Replaces the element at the specified position in this list with
     * the specified element.
     *
     * @param index index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public java_v.lang_v.Integer set(int index, java_v.lang_v.Integer element) {
        rangeCheck(index);

        java_v.lang_v.Integer oldValue = elementData(index);
        elementData[index] = element;
        return oldValue;
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param new_value element to be appended to this list
     * @return <tt>true</tt> (as specified by {@link Collection#add})
     */
    /*@ also
      @ assignable elementData[*], size;
      @
      @ ensures size == 1 + \old(size);
      @ ensures (\forall int i; 0 <= i && i < \old(size) ==> elementData[i] == \old(elementData[i]));
      @ ensures elementData[\old(size)] == new_value; 
      @*/
    public boolean add(java_v.lang_v.Integer new_value) {
        ensureCapacity(size + 1); // Increments modCount!!
        elementData[size++] = new_value;
        return true;
    }

    /**
     * Inserts the specified element at the specified position in this
     * list. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (adds one to their indices).
     *
     * @param index index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */

    public void add(int index, java_v.lang_v.Integer element) {
        rangeCheckForAdd(index);

        ensureCapacity(size + 1); // Increments modCount!!
        java_v.lang_v.System.arraycopyInteger(elementData, index, elementData,
                                              index + 1, size - index);
        elementData[index] = element;
        size++;
    }

    /**
     * Removes the element at the specified position in this list.
     * Shifts any subsequent elements to the left (subtracts one from their
     * indices).
     *
     * @param index the index of the element to be removed
     * @return the element that was removed from the list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public java_v.lang_v.Integer removeIndex(int index) {
        rangeCheck(index);

        modCount++;
        java_v.lang_v.Integer oldValue = elementData(index);

        int numMoved = size - index - 1;
        if (numMoved > 0)
            java_v.lang_v.System.arraycopyInteger(elementData, index + 1,
                                                  elementData, index, numMoved);
        elementData[--size] = null; // Let gc do its work

        return oldValue;
    }

    /**
     * Appends all of the elements in the specified collection to the end of
     * this list, in the order that they are returned by the
     * specified collection's Iterator.  The behavior of this operation is
     * undefined if the specified collection is modified while the operation
     * is in progress.  (This implies that the behavior of this call is
     * undefined if the specified collection is this list, and this
     * list is nonempty.)
     *
     * @param c collection containing elements to be added to this list
     * @return <tt>true</tt> if this list changed as a result of the call
     * @throws NullPointerException if the specified collection is null
     */
    public boolean addAll(Collection c) {
        java_v.lang_v.Integer[] a = c.toArray();
        int numNew = a.length;
        ensureCapacity(size + numNew); // Increments modCount
        java_v.lang_v.System.arraycopyInteger(a, 0, elementData, size, numNew);
        size += numNew;
        return numNew != 0;
    }

    /**
     * Inserts all of the elements in the specified collection into this
     * list, starting at the specified position.  Shifts the element
     * currently at that position (if any) and any subsequent elements to
     * the right (increases their indices).  The new elements will appear
     * in the list in the order that they are returned by the
     * specified collection's iterator.
     *
     * @param index index at which to insert the first element from the
     *              specified collection
     * @param c collection containing elements to be added to this list
     * @return <tt>true</tt> if this list changed as a result of the call
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @throws NullPointerException if the specified collection is null
     */
    public boolean addAll(int index, Collection c) {
        rangeCheckForAdd(index);

        java_v.lang_v.Integer[] a = c.toArray();
        int numNew = a.length;
        ensureCapacity(size + numNew); // Increments modCount

        int numMoved = size - index;
        if (numMoved > 0)
            java_v.lang_v.System.arraycopyInteger(elementData, index,
                                                  elementData, index + numNew, numMoved);

        java_v.lang_v.System.arraycopyInteger(a, 0, elementData, index, numNew);
        size += numNew;
        return numNew != 0;
    }

    /**
     * Checks if the given index is in range.  If not, throws an appropriate
     * runtime exception.  This method does *not* check if the index is
     * negative: It is always used immediately prior to an array access,
     * which throws an ArrayIndexOutOfBoundsException if index is negative.
     */
    private void rangeCheck(int index) {
        if (index >= size)
            throw new java_v.lang_v.IndexOutOfBoundsException();
    }

    /**
     * A version of rangeCheck used by add and addAll.
     */
    private void rangeCheckForAdd(int index) {
        if (index > size || index < 0)
            throw new java_v.lang_v.IndexOutOfBoundsException();
    }

    /**
     * Removes from this list all of its elements that are contained in the
     * specified collection.
     *
     * @param c collection containing elements to be removed from this list
     * @return {@code true} if this list changed as a result of the call
     * @throws ClassCastException if the class of an element of this list
     *         is incompatible with the specified collection (optional)
     * @throws NullPointerException if this list contains a null element and the
     *         specified collection does not permit null elements (optional),
     *         or if the specified collection is null
     * @see Collection#contains(Object)
     */
    public boolean removeAll(Collection c) {
        return batchRemove(c, false);

    }

    /**
     * Retains only the elements in this list that are contained in the
     * specified collection.  In other words, removes from this list all
     * of its elements that are not contained in the specified collection.
     *
     * @param c collection containing elements to be retained in this list
     * @return {@code true} if this list changed as a result of the call
     * @throws ClassCastException if the class of an element of this list
     *         is incompatible with the specified collection (optional)
     * @throws NullPointerException if this list contains a null element and the
     *         specified collection does not permit null elements (optional),
     *         or if the specified collection is null
     * @see Collection#contains(Object)
     */
    public boolean retainAll(Collection c) {
        return batchRemove(c, true);
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list.
     * The specified index indicates the first element that would be
     * returned by an initial call to {@link ListIterator#next next}.
     * An initial call to {@link ListIterator#previous previous} would
     * return the element with the specified index minus one.
     *
     * <p>The returned list iterator is <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public ListIterator listIterator(int index) {
        if (index < 0 || index > size)
            throw new java_v.lang_v.IndexOutOfBoundsException();
        return new ListItr(index);
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence).
     *
     * <p>The returned list iterator is <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @see #listIterator(int)
     */
    public ListIterator listIterator() {
        return new ListItr(0);
    }

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     *
     * <p>The returned iterator is <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    public Iterator iterator() {
        return new Itr();
    }

    /**
     * An optimized version of AbstractList.Itr
     */
    private class Itr implements Iterator {
        int cursor; // index of next element to return
        int lastRet = -1; // index of last element returned; -1 if no such
        int expectedModCount = modCount;

        public boolean hasNext() {
            return cursor != size;
        }

        public java_v.lang_v.Integer next() {
            checkForComodification();
            int i = cursor;
            if (i >= size)
                throw new NoSuchElementException();
            java_v.lang_v.Integer[] elementData = ArrayList.this.elementData;
            if (i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i + 1;
            return elementData[lastRet = i];
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();

            try {
                ArrayList.this.removeIndex(lastRet);
                cursor = lastRet;
                lastRet = -1;
                expectedModCount = modCount;
            } catch (java_v.lang_v.IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }

    /**
     * An optimized version of AbstractList.ListItr
     */
    private class ListItr extends Itr implements ListIterator {
        ListItr(int index) {
            super();
            cursor = index;
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public java_v.lang_v.Integer previous() {
            checkForComodification();
            int i = cursor - 1;
            if (i < 0)
                throw new NoSuchElementException();
            java_v.lang_v.Integer[] elementData = ArrayList.this.elementData;
            if (i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i;
            return elementData[lastRet = i];
        }

        public void set(java_v.lang_v.Integer e) {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();

            try {
                ArrayList.this.set(lastRet, e);
            } catch (java_v.lang_v.IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void add(java_v.lang_v.Integer e) {
            checkForComodification();

            try {
                int i = cursor;
                ArrayList.this.add(i, e);
                cursor = i + 1;
                lastRet = -1;
                expectedModCount = modCount;
            } catch (java_v.lang_v.IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
    }

    /**
     * Returns a view of the portion of this list between the specified
     * {@code fromIndex}, inclusive, and {@code toIndex}, exclusive.  (If
     * {@code fromIndex} and {@code toIndex} are equal, the returned list is
     * empty.)  The returned list is backed by this list, so non-structural
     * changes in the returned list are reflected in this list, and vice-versa.
     * The returned list supports all of the optional list operations.
     *
     * <p>This method eliminates the need for explicit range operations (of
     * the sort that commonly exist for arrays).  Any operation that expects
     * a list can be used as a range operation by passing a subList view
     * instead of a whole list.  For example, the following idiom
     * removes a range of elements from a list:
     * <pre>
     *      list.subList(from, to).clear();
     * </pre>
     * Similar idioms may be constructed for {@link #indexOf(Object)} and
     * {@link #lastIndexOf(Object)}, and all of the algorithms in the
     * {@link Collections} class can be applied to a subList.
     *
     * <p>The semantics of the list returned by this method become undefined if
     * the backing list (i.e., this list) is <i>structurally modified</i> in
     * any way other than via the returned list.  (Structural modifications are
     * those that change the size of this list, or otherwise perturb it in such
     * a fashion that iterations in progress may yield incorrect results.)
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @throws IllegalArgumentException {@inheritDoc}
     */
    public List subList(int fromIndex, int toIndex) {
        subListRangeCheck(fromIndex, toIndex, size);
        return new SubList(this, 0, fromIndex, toIndex);
    }

    static void subListRangeCheck(int fromIndex, int toIndex, int size) {
        if (fromIndex < 0)
            throw new java_v.lang_v.IndexOutOfBoundsException();
        if (toIndex > size)
            throw new java_v.lang_v.IndexOutOfBoundsException();
        if (fromIndex > toIndex)
            throw new java_v.lang_v.IllegalArgumentException();
    }

    private class SubListIterator implements ListIterator {
        int cursor;
        int lastRet;
        int expectedModCount;
        SubList subList;

        private SubListIterator(int index, SubList subList) {
            cursor = index;
            lastRet = -1;
            expectedModCount = ArrayList.this.modCount;
            this.subList = subList;
        }

        public boolean hasNext() {
            return cursor != subList.size;
        }

        public java_v.lang_v.Integer next() {
            checkForComodification();
            int i = cursor;
            if (i >= subList.size)
                throw new NoSuchElementException();
            java_v.lang_v.Integer[] elementData = ArrayList.this.elementData;
            if (subList.offset + i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i + 1;
            return elementData[subList.offset + (lastRet = i)];
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public java_v.lang_v.Integer previous() {
            checkForComodification();
            int i = cursor - 1;
            if (i < 0)
                throw new NoSuchElementException();
            java_v.lang_v.Integer[] elementData = ArrayList.this.elementData;
            if (subList.offset + i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i;
            return elementData[subList.offset + (lastRet = i)];
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();

            try {
                subList.removeIndex(lastRet);
                cursor = lastRet;
                lastRet = -1;
                expectedModCount = ArrayList.this.modCount;
            } catch (java_v.lang_v.IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void set(java_v.lang_v.Integer e) {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();

            try {
                ArrayList.this.set(subList.offset + lastRet, e);
            } catch (java_v.lang_v.IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void add(java_v.lang_v.Integer e) {
            checkForComodification();

            try {
                int i = cursor;
                subList.add(i, e);
                cursor = i + 1;
                lastRet = -1;
                expectedModCount = ArrayList.this.modCount;
            } catch (java_v.lang_v.IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        final void checkForComodification() {
            if (expectedModCount != ArrayList.this.modCount)
                throw new ConcurrentModificationException();
        }
    }

    private class SubList extends AbstractList implements RandomAccess {
        private final AbstractList parent;
        private final int parentOffset;
        private final int offset;
        private int size;

        SubList(AbstractList parent, int offset, int fromIndex, int toIndex) {
            this.parent = parent;
            this.parentOffset = fromIndex;
            this.offset = offset + fromIndex;
            this.size = toIndex - fromIndex;
            this.modCount = ArrayList.this.modCount;
        }

        public java_v.lang_v.Integer set(int index, java_v.lang_v.Integer e) {
            rangeCheck(index);
            checkForComodification();
            java_v.lang_v.Integer oldValue = ArrayList.this.elementData(offset
                                                                        + index);
            ArrayList.this.elementData[offset + index] = e;
            return oldValue;
        }

        public java_v.lang_v.Integer get(int index) {
            rangeCheck(index);
            checkForComodification();
            return ArrayList.this.elementData(offset + index);
        }

        public int size() {
            checkForComodification();
            return this.size;
        }

        public void add(int index, java_v.lang_v.Integer e) {
            rangeCheckForAdd(index);
            checkForComodification();
            parent.add(parentOffset + index, e);
            this.modCount = parent.modCount;
            this.size++;
        }

        public java_v.lang_v.Integer removeIndex(int index) {
            rangeCheck(index);
            checkForComodification();
            java_v.lang_v.Integer result = parent.removeIndex(parentOffset
                                                              + index);
            this.modCount = parent.modCount;
            this.size--;
            return result;
        }

        protected void removeRange(int fromIndex, int toIndex) {
            checkForComodification();
            parent.removeRange(parentOffset + fromIndex, parentOffset + toIndex);
            this.modCount = parent.modCount;
            this.size -= toIndex - fromIndex;
        }

        public boolean addAll(Collection c) {
            return addAll(this.size, c);
        }

        public boolean addAll(int index, Collection c) {
            rangeCheckForAdd(index);
            int cSize = c.size();
            if (cSize == 0)
                return false;

            checkForComodification();
            parent.addAll(parentOffset + index, c);
            this.modCount = parent.modCount;
            this.size += cSize;
            return true;
        }

        public Iterator iterator() {
            return listIterator();
        }

        public ListIterator listIterator(final int index) {
            checkForComodification();
            rangeCheckForAdd(index);
            return new SubListIterator(index, this);
        }

        public List subList(int fromIndex, int toIndex) {
            subListRangeCheck(fromIndex, toIndex, size);
            return new SubList(this, offset, fromIndex, toIndex);
        }

        private void rangeCheck(int index) {
            if (index < 0 || index >= this.size)
                throw new java_v.lang_v.IndexOutOfBoundsException();
        }

        private void rangeCheckForAdd(int index) {
            if (index < 0 || index > this.size)
                throw new java_v.lang_v.IndexOutOfBoundsException();
        }

        private void checkForComodification() {
            if (ArrayList.this.modCount != this.modCount)
                throw new ConcurrentModificationException();
        }
    }

    /**
     * The specification of this method is intentionally incorrect to
     * allow Daikon explore internal state of an ArrayList.  This
     * method is not used in practice.
     * 
     * When checking this method with ESCJava2 two warnings should
     * appear: 1) The method is not pure, and 2) The postcondition
     * doesn't hold
     * @return
     */
    //@ ensures \result == this.elementData;
    public/*@ pure @*/java_v.lang_v.Integer[] getElementData() {
        java_v.lang_v.Integer[] copyOf = Arrays.copyOf(elementData,
                                                       elementData.length);
        return copyOf;
    }

    /**
     * The only purpose of this method is allowing 
     * Chicory to access the private fields of this class.
     * 
     */
    private void unreachable_method() {
    }

}
