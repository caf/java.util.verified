/*
 * COPYRIGHT NOTICE FOR JAVA CODE:
 *
 * Copyright 1997-2007 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 */
 
 
/*
 * COPYRIGHT NOTICE FOR JML ANNOTATIONS:
 *
 * Copyright 2013 Carlo A. Furia and Juan Pablo Galeotti.
 *
 * The JML annotations are free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 */
 


package java_v.util_v;

import java_v.theories.TArrays;


public abstract class Arrays {

    /**
     * Searches the specified array of ints for the specified value using the
     * binary search algorithm. The array must be sorted (as by the
     * {@link #sort(int[])} method) prior to making this call. If it is not
     * sorted, the results are undefined. If the array contains multiple
     * elements with the specified value, there is no guarantee which one will
     * be found.
     * 
     * @param a
     *            the array to be searched
     * @param key
     *            the value to be searched for
     * @return index of the search key, if it is contained in the array;
     *         otherwise, <tt>(-(<i>insertion point</i>) - 1)</tt>. The
     *         <i>insertion point</i> is defined as the point at which the key
     *         would be inserted into the array: the index of the first element
     *         greater than the key, or <tt>a.length</tt> if all elements in the
     *         array are less than the specified key. Note that this guarantees
     *         that the return value will be &gt;= 0 if and only if the key is
     *         found.
     */
    /*@ 
      @ requires a != null; 
      @*/
    public static int binarySearch(int[] a, int key) {
        return binarySearch0(a, 0, a.length, key);
    }

    /**
     * Searches a range of the specified array of ints for the specified value
     * using the binary search algorithm. The range must be sorted (as by the
     * {@link #sort(int[], int, int)} method) prior to making this call. If it
     * is not sorted, the results are undefined. If the range contains multiple
     * elements with the specified value, there is no guarantee which one will
     * be found.
     * 
     * @param a
     *            the array to be searched
     * @param fromIndex
     *            the index of the first element (inclusive) to be searched
     * @param toIndex
     *            the index of the last element (exclusive) to be searched
     * @param key
     *            the value to be searched for
     * @return index of the search key, if it is contained in the array within
     *         the specified range; otherwise,
     *         <tt>(-(<i>insertion point</i>) - 1)</tt>. The <i>insertion
     *         point</i> is defined as the point at which the key would be
     *         inserted into the array: the index of the first element in the
     *         range greater than the key, or <tt>toIndex</tt> if all elements
     *         in the range are less than the specified key. Note that this
     *         guarantees that the return value will be &gt;= 0 if and only if
     *         the key is found.
     * @throws IllegalArgumentException
     *             if {@code fromIndex > toIndex}
     * @throws ArrayIndexOutOfBoundsException
     *             if {@code fromIndex < 0 or toIndex > a.length}
     * @since 1.6
     */
    public static int binarySearch(int[] a, int fromIndex, int toIndex, int key) {
        rangeCheck(a.length, fromIndex, toIndex);
        return binarySearch0(a, fromIndex, toIndex, key);
    }

    /*@
      @ requires a != null;
      @ requires TArrays.within(a,fromIndex,toIndex);
      @ requires TArrays.sorted(a,fromIndex,toIndex);
      @
      @ ensures \result >= 0 ==> a[\result] == key;
      @ ensures \result < 0 ==> (!TArrays.in(a,fromIndex,toIndex,key));
      @*/
    private static/*@ pure @*/int binarySearch0(int[] a, int fromIndex,
                                                int toIndex, int key) {

        int low = fromIndex;
        int high = toIndex - 1;

        //@ loop_invariant 0 <= fromIndex && fromIndex <= toIndex && toIndex<=a.length;
        //@ loop_invariant high >= -1;
        //@ loop_invariant fromIndex <= low;
        //@ loop_invariant low <= high +1;
        //@ loop_invariant high < toIndex;
        //@ loop_invariant TArrays.sorted(a, fromIndex, toIndex);
        //@ loop_invariant TArrays.less(a, fromIndex, low, key);
        //@ loop_invariant TArrays.grt(a, high + 1, toIndex, key);
        while (low <= high) {
            int mid = low + ((high - low) / 2); // equivalent to (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal < key) {
                low = mid + 1;
            } else if (midVal > key) {
                high = mid - 1;
            } else {
                return mid; // key found
            }
        }
        return -(low + 1); // key not found.

    }

    /**
     * Copies the specified array, truncating or padding with zeros (if
     * necessary) so the copy has the specified length. For all indices that are
     * valid in both the original array and the copy, the two arrays will
     * contain identical values. For any indices that are valid in the copy but
     * not the original, the copy will contain <tt>0</tt>. Such indices will
     * exist if and only if the specified length is greater than that of the
     * original array.
     * 
     * @param original
     *            the array to be copied
     * @param newLength
     *            the length of the copy to be returned
     * @return a copy of the original array, truncated or padded with zeros to
     *         obtain the specified length
     * @throws NegativeArraySizeException
     *             if <tt>newLength</tt> is negative
     * @throws NullPointerException
     *             if <tt>original</tt> is null
     * @since 1.6
     */
    /*@ 
      @ requires original != null;
      @ requires newLength >= 0;
      @ 
      @ assignable original[*];
      @
      @ ensures \result != null;
      @ ensures (\forall int i; 0 <= i && i < original.length ==> original[i] == \old(original[i]));
      @ ensures (\forall int i; original.length <= i && i < newLength ==> \result[i] == 0);
      @ ensures \result.length == newLength;
      @ ensures \result.length <= original.length ==>
      @                  (\forall int i; 0 <= i && i < \result.length ==> \result[i] == original[i]);
      @ ensures \result.length > original.length ==>
      @                  (\forall int i; 0 <= i && i < original.length ==> \result[i] == original[i]);
      @*/
    public static int[] copyOf(int[] original, int newLength) {
        int[] copy = new int[newLength];
        int min_length = Math.min(original.length, newLength);
        java_v.lang_v.System.arraycopy(original, 0, copy, 0, min_length);
        return copy;
    }

    /*@ 
      @ requires original != null;
      @ requires newLength >= 0;
      @ 
      @ assignable original[*];
      @
      @ ensures \result != null;
      @ ensures (\forall int i; 0 <= i && i < original.length ==> original[i] == \old(original[i]));
      @ ensures (\forall int i; original.length <= i && i < newLength ==> \result[i] == null);
      @ ensures \result.length == newLength;
      @ ensures \result.length <= original.length ==>
      @                  (\forall int i; 0 <= i && i < \result.length ==> \result[i] == original[i]);
      @ ensures \result.length > original.length ==>
      @                  (\forall int i; 0 <= i && i < original.length ==> \result[i] == original[i]);
      @*/
    public static java_v.lang_v.Integer[] copyOf(java_v.lang_v.Integer[] original,
                                                 int newLength) {
        java_v.lang_v.Integer[] copy = new java_v.lang_v.Integer[newLength];
        java_v.lang_v.System.arraycopyInteger(original, 0, copy, 0,
                                              Math.min(original.length, newLength));
        return copy;
    }

    /**
     * Copies the specified range of the specified array into a new array. The
     * initial index of the range (<tt>from</tt>) must lie between zero and
     * <tt>original.length</tt>, inclusive. The value at <tt>original[from]</tt>
     * is placed into the initial element of the copy (unless
     * <tt>from == original.length</tt> or <tt>from == to</tt>). Values from
     * subsequent elements in the original array are placed into subsequent
     * elements in the copy. The final index of the range (<tt>to</tt>), which
     * must be greater than or equal to <tt>from</tt>, may be greater than
     * <tt>original.length</tt>, in which case <tt>0</tt> is placed in all
     * elements of the copy whose index is greater than or equal to
     * <tt>original.length - from</tt>. The length of the returned array will be
     * <tt>to - from</tt>.
     * 
     * @param original
     *            the array from which a range is to be copied
     * @param from
     *            the initial index of the range to be copied, inclusive
     * @param to
     *            the final index of the range to be copied, exclusive. (This
     *            index may lie outside the array.)
     * @return a new array containing the specified range from the original
     *         array, truncated or padded with zeros to obtain the required
     *         length
     * @throws ArrayIndexOutOfBoundsException
     *             if {@code from < 0} or {@code from > original.length}
     * @throws IllegalArgumentException
     *             if <tt>from &gt; to</tt>
     * @throws NullPointerException
     *             if <tt>original</tt> is null
     * @since 1.6
     */
    public static int[] copyOfRange(int[] original, int from, int to) {
        int newLength = to - from;
        if (newLength < 0)
            throw new java_v.lang_v.IllegalArgumentException();
        int[] copy = new int[newLength];
        java_v.lang_v.System.arraycopy(original, from, copy, 0,
                                       Math.min(original.length - from, newLength));
        return copy;
    }

    /**
     * Assigns the specified int value to each element of the specified range of
     * the specified array of ints. The range to be filled extends from index
     * <tt>fromIndex</tt>, inclusive, to index <tt>toIndex</tt>, exclusive. (If
     * <tt>fromIndex==toIndex</tt>, the range to be filled is empty.)
     * 
     * @param a
     *            the array to be filled
     * @param fromIndex
     *            the index of the first element (inclusive) to be filled with
     *            the specified value
     * @param toIndex
     *            the index of the last element (exclusive) to be filled with
     *            the specified value
     * @param val
     *            the value to be stored in all elements of the array
     * @throws IllegalArgumentException
     *             if <tt>fromIndex &gt; toIndex</tt>
     * @throws ArrayIndexOutOfBoundsException
     *             if <tt>fromIndex &lt; 0</tt> or
     *             <tt>toIndex &gt; a.length</tt>
     */
    // Called "fill_b" in http://arxiv.org/abs/1407.5286
    /*@ normal_behavior
      @ requires a != null;
      @ requires TArrays.within(a, fromIndex, toIndex);
      @ assignable a[*];
      @ ensures (\forall int j; 0 <= j && j < fromIndex ==> a[j] == \old(a[j]));
      @ ensures TArrays.eq(a, fromIndex, toIndex, val);
      @ ensures (\forall int j; toIndex <= j && j < a.length ==> a[j] == \old(a[j]));
      @
      @ also   exceptional_behavior
      @ requires fromIndex > toIndex;
      @ signals (java_v.lang_v.IllegalArgumentException ex) true;
      @
      @ also exceptional_behavior
      @ requires fromIndex < 0 || toIndex > a.length;
      @ signals (java_v.lang_v.ArrayIndexOutOfBoundsException  ex) true;
      @
      @ also exceptional_behavior
      @ requires a == null;
      @ signals (NullPointerException ex) true;
      @*/
    public static void fill1(int[] a, int fromIndex, int toIndex, int val)
        throws java_v.lang_v.IllegalArgumentException,
               java_v.lang_v.ArrayIndexOutOfBoundsException, NullPointerException {
        if (a == null)
            throw new NullPointerException();

        rangeCheck(a.length, fromIndex, toIndex);

        int ic = fromIndex;

        //@ loop_invariant fromIndex <= ic && ic <= toIndex;
        //@ loop_invariant (\forall int j; 0 <= j && j < fromIndex ==> a[j] == \old(a[j]));
        //@ loop_invariant TArrays.eq(a, fromIndex, ic, val);
        //@ loop_invariant (\forall int j; toIndex <= j && j < a.length ==> a[j] == \old(a[j]));
        for (; ic < toIndex; ic++) {
            a[ic] = val;
        }
    }

    /**
     * Assigns the specified int value to each element of the specified array of
     * ints.
     * 
     * @param a
     *            the array to be filled
     * @param val
     *            the value to be stored in all elements of the array
     */
    // Called "fill_a" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ requires a != null;
      @
      @ assignable a[*];
      @
      @ ensures TArrays.eq(a, 0, a.length, val); 
      @*/
    public static void fill0(int[] a, int val) {

        int ic = 0;
        int len = a.length;

        //@ loop_invariant 0 <= ic && ic <= a.length;
        //@ loop_invariant TArrays.eq(a, 0, ic, val);
        for (; ic < len; ic++) {
            a[ic] = val;
        }
    }

    public static void sort(int[] a, int fromIndex, int toIndex) {
    }

    /*@
      @ assignable a[*];
      @ ensures (\forall int i; 0 <= i && i < a.length; a[i] != null);
      @ ensures (\forall int i; 0 <= i && i < a.length - 1; a[i].intValue() <= a[i + 1].intValue());
      @*/
    public static void sortInteger(java_v.lang_v.Integer[] a) {
        java_v.lang_v.Integer[] aux = (java_v.lang_v.Integer[]) a.clone();
        mergeSort(aux, a, 0, a.length, 0);
    }

    public static void sortInteger(java_v.lang_v.Integer[] a, int fromIndex,
                                   int toIndex) {
        java_v.lang_v.Integer[] aux = (java_v.lang_v.Integer[]) a.clone();
        mergeSort(aux, a, fromIndex, toIndex, -fromIndex);
    }

    public static void sort(int[] a) {
        sort1(a, 0, a.length);
    }

    /**
     * Sorts the specified sub-array of integers into ascending order.
     */
    private static void sort1(int x[], int off, int len) {
        // Insertion sort on smallest arrays
        if (len < 7) {
            insertionSort1(x, off, len);
            return;
        }

        // Choose a partition element, v
        int m = off + MathAux.signed_right_shift(len, 1); // Small arrays, middle element
        if (len > 7) {
            int l = off;
            int n = off + len - 1;
            if (len > 40) { // Big arrays, pseudomedian of 9
                int s = len / 8;
                l = med3(x, l, l + s, l + 2 * s);
                m = med3(x, m - s, m, m + s);
                n = med3(x, n - 2 * s, n - s, n);
            }
            m = med3(x, l, m, n); // Mid-size, med of 3
        }
        int v = x[m];

        // Establish Invariant: v* (<v)* (>v)* v*
        PartitionIndexes idxs = quicksortPartition(x, off, len, v);
        int a = idxs.a;
        int b = idxs.b;
        int c = idxs.c;
        int d = idxs.d;

        // Swap partition elements back to middle
        int s, n = off + len;
        s = Math.min(a - off, b - a);
        vecswap(x, off, b - s, s);
        s = Math.min(d - c, n - d - 1);
        vecswap(x, b, n - s, s);

        // Recursively sort non-partition-elements
        if ((s = b - a) > 1)
            sort1(x, off, s);
        if ((s = d - c) > 1)
            sort1(x, n - s, s);
    }

    /*@ 
      @ requires x != null;
      @ requires len >= 0;
      @ requires off >=0;
      @ requires off + len <= x.length;
      @
      @ assignable x[*]; 
      @
      @ ensures (\forall int i; 0 <= i && i < off ==> x[i] == \old(x[i]));
      @ ensures (\forall int i; off + len <= i && i < x.length ==> x[i] == \old(x[i]));
      @ ensures TArrays.eq(x, off, \result.a, v);
      @ ensures TArrays.less(x, \result.a, \result.b, v);
      @ ensures TArrays.grt(x, \result.c + 1, \result.d + 1, v);
      @ ensures TArrays.eq(x, \result.d + 1, off + len, v);
      @ ensures off <= \result.a;
      @ ensures \result.a <= \result.b;
      @ ensures \result.b == \result.c + 1;
      @ ensures \result.c <= \result.d;
      @ ensures \result.d < off + len;
      @*/
    private static PartitionIndexes quicksortPartition(int[] x, int off,
                                                       int len, int v) {
        int a = off, b = a, c = off + len - 1, d = c;
      
        //@ loop_invariant d < off + len; 
        //@ loop_invariant b <= c + 1; 
        //@ loop_invariant off <= a;
        //@ loop_invariant a <= b;
        //@ loop_invariant c <= d;
        //@ loop_invariant (\forall int i; 0 <= i && i < off ==> x[i] == \old(x[i]));
        //@ loop_invariant (\forall int i; off + len <= i && i < x.length ==> x[i] == \old(x[i]));
        //@ loop_invariant x != null && 0 <= off && off <= a && a <= x.length && (\forall int jj; off <= jj && jj < a ==> x[jj] == v);
        //@ loop_invariant x != null && 0 <= a && a <= b && b <= x.length && (\forall int jj; a <= jj && jj < b ==> x[jj] < v);
        //@ loop_invariant x != null && 0 <= c + 1 && c + 1 <= d + 1 && d + 1 <= x.length && (\forall int jj; c + 1 <= jj && jj < d + 1 ==> x[jj] > v);
        //@ loop_invariant x != null && 0 <= d + 1 && d + 1 <= off + len && off + len <= x.length && (\forall int jj; d + 1 <= jj && jj < off + len ==> x[jj] == v);
        while (true) {

            //@ loop_invariant b <= c + 1; 
            //@ loop_invariant off <= a;
            //@ loop_invariant a <= b;
            //@ loop_invariant c <= d;
            //@ loop_invariant (\forall int i; 0 <= i && i < off ==> x[i] == \old(x[i]));
            //@ loop_invariant (\forall int i; off + len <= i && i < x.length ==> x[i] == \old(x[i]));
            //@ loop_invariant x != null && 0 <= off && off <= a && a <= x.length && (\forall int jj; off <= jj && jj < a ==> x[jj] == v);
            //@ loop_invariant x != null && 0 <= a && a <= b && b <= x.length && (\forall int jj; a <= jj && jj < b ==> x[jj] < v);
            //@ loop_invariant x != null && 0 <= c + 1 && c + 1 <= d + 1 && d + 1 <= x.length && (\forall int jj; c + 1 <= jj && jj < d + 1 ==> x[jj] > v);
            //@ loop_invariant x != null && 0 <= d + 1 && d + 1 <= off + len && off + len <= x.length && (\forall int jj; d + 1 <= jj && jj < off + len ==> x[jj] == v);
            while (b <= c && x[b] <= v) {
                if (x[b] == v) {
                    swap(x, a++, b);
                }
                b++;
            }

            //@ loop_invariant b <= c ==> x[b] > v; 
            //@ loop_invariant b <= c + 1; 
            //@ loop_invariant off <= a;
            //@ loop_invariant a <= b;
            //@ loop_invariant c <= d;
            //@ loop_invariant (\forall int i; 0 <= i && i < off ==> x[i] == \old(x[i]));
            //@ loop_invariant (\forall int i; off + len <= i && i < x.length ==> x[i] == \old(x[i]));
            //@ loop_invariant x != null && 0 <= off && off <= a && a <= x.length && (\forall int jj; off <= jj && jj < a ==> x[jj] == v);
            //@ loop_invariant x != null && 0 <= a && a <= b && b <= x.length && (\forall int jj; a <= jj && jj < b ==> x[jj] < v);
            //@ loop_invariant x != null && 0 <= c + 1 && c + 1 <= d + 1 && d + 1 <= x.length && (\forall int jj; c + 1 <= jj && jj < d + 1 ==> x[jj] > v);
            //@ loop_invariant x != null && 0 <= d + 1 && d + 1 <= off + len && off + len <= x.length && (\forall int jj; d + 1 <= jj && jj < off + len ==> x[jj] == v);
            while (c >= b && x[c] >= v) {
                if (x[c] == v) {
                    swap(x, c, d--);
                }
                c--;
            }
            if (b > c) {
                break;
            }
            swap(x, b++, c--);
        }
        PartitionIndexes idxs = new PartitionIndexes();
        idxs.a = a;
        idxs.b = b;
        idxs.c = c;
        idxs.d = d;
        return idxs;
    }

    // Called "insertionSort_b" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ requires x != null;
      @ requires len >= 0;
      @ requires TArrays.within(x, off, off + len);
      @
      @ assignable x[*];
      @
      @ ensures TArrays.sorted(x, off, off + len);
      @*/
    private static void insertionSort1(int[] x, int off, int len) {
        if (x == null)
            throw new NullPointerException();

        if (off < 0 || len > x.length || len < 0 || off > x.length
            || off + len > x.length)
            throw new RuntimeException();

        if (len == 0)
            return;

        if (len == 1)
            return;

        int high = off + len;

        int ic = off + 1;
        //@ loop_invariant off + 1 <= ic && ic <= high;
        //@ loop_invariant high == off + len;
        //@ loop_invariant TArrays.sorted(x, off, ic);
        while (ic < high) {
            int new_value = x[ic];

            x[ic] = x[ic - 1];

            int jc = ic;
            //@ loop_invariant off <= jc && jc <= ic;
            //@ loop_invariant off < ic && ic < high;
            //@ loop_invariant high == off + len;
            //@ loop_invariant TArrays.sorted(x, off, jc);
            //@ loop_invariant !(jc == ic) ==> TArrays.lesseq(x, off, jc, x[jc + 1]);
            //@ loop_invariant TArrays.grt(x, jc + 1, ic + 1, new_value);
            //@ loop_invariant TArrays.sorted(x, jc + 1, ic + 1);
            while (jc > off && x[jc - 1] > new_value) {

                x[jc] = x[jc - 1];
                jc--;
            }

            x[jc] = new_value;
            ic++;

        }

    }

    /**
     * Swaps x[a] with x[b].
     */
    /*@ 
      @ requires x != null;
      @ requires TArrays.in_bound(x, a) && TArrays.in_bound(x, b); 
      @
      @ assignable x[a], x[b];
      @
      @ ensures x[a] == \old(x[b]);
      @ ensures x[b] == \old(x[a]);
      @ ensures (\forall int i; 0 <= i && i < x.length && i != a && i != b ==> x[i] == \old(x[i]));
      @*/
    private static void swap(int x[], int a, int b) {
        int t = x[a];
        x[a] = x[b];
        x[b] = t;
    }

    /**
     * Returns the index of the median of the three indexed integers.
     */
    private static int med3(int x[], int a, int b, int c) {
        return (x[a] < x[b] ? (x[b] < x[c] ? b : x[a] < x[c] ? c : a)
                : (x[b] > x[c] ? b : x[a] > x[c] ? c : a));
    }

    /**
     * Swaps x[a .. (a+n-1)] with x[b .. (b+n-1)].
     */
    /*@ 
      @ requires x != null;
      @ requires n >= 0;
      @ requires TArrays.within(x, a, a + n);
      @ requires TArrays.within(x, b, b + n);
      // intervals are disjoint
      @ requires a + n <= b || b + n <= a;
      @
      @ assignable x[*];
      @
      @ ensures (\forall int j; 0 <= j && j < (a < b ? a : b) ==> x[j] == \old(x[j]));
      @ ensures (\forall int j; (a > b ? a : b) + n <= j && j < x.length ==> x[j] == \old(x[j]));
      @ ensures (\forall int j; 0 <= j && j < n ==> x[a + j] == \old(x[b + j]));
      @ ensures (\forall int j; 0 <= j && j < n ==> x[b + j] == \old(x[a + j]));
      @*/
    private static void vecswap(int x[], int a, int b, int n) {

        int ic = 0;

        //@ loop_invariant 0 <= ic && ic <= n;
        //@ loop_invariant (\forall int j; 0 <= j && j < (a < b ? a : b) ==> x[j] == \old(x[j]));
        //@ loop_invariant (\forall int j; (a > b ? a : b) + n <= j && j < x.length ==> x[j] == \old(x[j]));
        //@ loop_invariant (\forall int j; 0 <= j && j < ic ==> x[a + j] == \old(x[b + j]));
        //@ loop_invariant (\forall int j; 0 <= j && j < ic ==> x[b + j] == \old(x[a + j]));
        //@ loop_invariant (\forall int j; a + ic <= j && j < a + n ==> x[j] == \old(x[j]));
        //@ loop_invariant (\forall int j; b + ic <= j && j < b + n ==> x[j] == \old(x[j]));
        for (; ic < n; ic++) {
            swap(x, a + ic, b + ic);
        }
    }

    /**
     * Check that fromIndex and toIndex are in range, and throw an appropriate
     * exception if they aren't.
     */
    /*@ normal_behavior
      @ requires 0 <= fromIndex && fromIndex <= toIndex && toIndex <= arrayLen;
      @
      @ also exceptional_behavior
      @ requires fromIndex > toIndex;
      @ signals (java_v.lang_v.IllegalArgumentException ex) true;
      @
      @ also exceptional_behavior
      @ requires fromIndex < 0 || toIndex > arrayLen;
      @ signals (java_v.lang_v.ArrayIndexOutOfBoundsException ex) true;
      @*/
    private static/*@ pure @*/void rangeCheck(int arrayLen, int fromIndex,
                                              int toIndex) throws java_v.lang_v.IllegalArgumentException,
                                                                  java_v.lang_v.ArrayIndexOutOfBoundsException {
        if (fromIndex > toIndex)
            throw new java_v.lang_v.IllegalArgumentException();
        if (fromIndex < 0)
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();
        if (toIndex > arrayLen)
            throw new java_v.lang_v.ArrayIndexOutOfBoundsException();
    }

    /**
     * Tuning parameter: list size at or below which insertion sort will be
     * used in preference to mergesort or quicksort.
     */
    private static final int INSERTIONSORT_THRESHOLD = 7;

    /**
     * Src is the source array that starts at index 0
     * Dest is the (possibly larger) array destination with a possible offset
     * low is the index in dest to start sorting
     * high is the end index in dest to end sorting
     * off is the offset to generate corresponding low, high in src
     */

    private static void mergeSort(java_v.lang_v.Integer[] src,
                                  java_v.lang_v.Integer[] dest, int low, int high, int off) {
        int length = high - low;

        // Insertion sort on smallest arrays
        if (length < INSERTIONSORT_THRESHOLD) {
            insertionSort0(dest, low, high);
            return;
        }

        // Recursively sort halves of dest into src
        int destLow = low;
        int destHigh = high;
        low += off;
        high += off;
        int mid = MathAux.unsigned_right_shift(low + high, 1);
        mergeSort(dest, src, low, mid, -off);
        mergeSort(dest, src, mid, high, -off);

        // If list is already sorted, just copy from src to dest.  This is an
        // optimization that results in faster sorts for nearly ordered lists.
        if (src[mid - 1].intValue() <= src[mid].intValue()) {
            java_v.lang_v.System
                .arraycopyInteger(src, low, dest, destLow, length);
            return;
        }

        merge0(src, dest, low, mid, high, destLow, destHigh);
    }

    // Called "merge" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ requires src != null && dest != null;
      // Given the comment at the first instruction:
      @ requires TArrays.sorted(src, low, mid) && TArrays.sorted(src, mid, high);
      @ requires TArrays.within(src, low, high);
      @ requires low <= mid && mid <= high;
      @ requires TArrays.within(dest, destLow, destHigh);
      @ requires high - low == destHigh - destLow;
      @ requires TArrays.nonnull(src, low, high);
      // Source and destination are disjoint
      @ requires src == dest ==> high <= destLow || destHigh <= low;
      @
      @ assignable dest[*];
      @
      @ ensures TArrays.sorted(dest, destLow, destHigh);
      @*/
    private static void merge0(java_v.lang_v.Integer[] src,
                               java_v.lang_v.Integer[] dest, final int low, final int mid,
                               final int high, final int destLow, final int destHigh) {
        // Merge sorted halves (now in src) into dest
        int ic = destLow, p = low, q = mid;

        //@ loop_invariant destLow <= ic && ic <= destHigh;
        //@ loop_invariant destHigh - ic == (mid - p) + (high - q);
        //@ loop_invariant low <= p && p <= mid && mid <= q && q <= high;
        //@ loop_invariant 0 <= low && low <= high && high <= src.length;
        //@ loop_invariant 0 <= destLow && destLow <= destHigh && destHigh <= dest.length;
        //@ loop_invariant TArrays.nonnull(src, p, mid);
        //@ loop_invariant TArrays.nonnull(src, q, high);
        //@ loop_invariant TArrays.nonnull(dest, destLow, ic);
        // Not using predicates for speed of static checking
        //@ loop_invariant (\forall int i, j; p <= i && i < j && j < mid ==> src[i].intValue() <= src[j].intValue());
        //@ loop_invariant (\forall int i, j; q <= i && i < j && j < high ==> src[i].intValue() <= src[j].intValue());
        //@ loop_invariant p < mid ==> (\forall int j; destLow <= j && j < ic ==> dest[j].intValue() <= src[p].intValue());
        //@ loop_invariant q < high ==> (\forall int j; destLow <= j && j < ic ==> dest[j].intValue() <= src[q].intValue());
        //@ loop_invariant (\forall int i, j; destLow <= i && i < j && j < ic ==> dest[i].intValue() <= dest[j].intValue());
        //@ loop_invariant destHigh - ic == (mid - p) + (high - q); 
        for (; ic < destHigh; ic++) {
            if (q >= high || p < mid
                && (src[p].intValue() <= src[q].intValue()))
                dest[ic] = src[p++];
            else
                dest[ic] = src[q++];
        }
    }

    // Called "insertionSort_a" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ requires dest != null;
      @ requires TArrays.within(dest, low, high);
      @ requires TArrays.nonnull(dest, low, high);
      @
      @ assignable dest[*];
      @
      @ ensures TArrays.sorted(dest, low, high);
      @*/
    private static void insertionSort0(java_v.lang_v.Integer[] dest, int low,
                                       int high) {
        if (dest.length == 0)
            return;
        if (dest.length == 1)
            return;

        if (low == high) {
            return;
        }

        int ic = low + 1;

        //@ loop_invariant dest != null;                                   
        //@ loop_invariant high <= dest.length;
        //@ loop_invariant high >= low;                           
        //@ loop_invariant ic > low;                                   
        //@ loop_invariant ic >= 1;                                   
        //@ loop_invariant low <= dest.length;                                   
        //@ loop_invariant low >= 0;                                   
        //@ loop_invariant dest != null && 0 <= low && low <= ic && ic <= dest.length && (\forall int jj; low <= jj && jj < ic ==> dest[jj] != null) && (\forall int jj1, jj2; low <= jj1 && jj1 < jj2 && jj2 < ic ==> dest[jj1].intValue() <= dest[jj2].intValue());
        //@ loop_invariant dest != null && 0 <= low && low <= high && high <= dest.length && (\forall int jj; low <= jj && jj < high ==> dest[jj] != null);
        while (ic < high) {

            java_v.lang_v.Integer new_value = dest[ic];
            dest[ic] = dest[ic - 1];
            int jc = ic;

            //@ loop_invariant low <= jc && jc <= ic;           
            //@ loop_invariant low < ic && ic < high;          
            //@ loop_invariant dest != null && 0 <= jc + 1 && jc + 1 <= ic + 1 && ic + 1 <= dest.length && (\forall int jj; jc + 1 <= jj && jj < ic + 1 ==> dest[jj] != null) && new_value != null && (\forall int jj; jc + 1 <= jj && jj < ic + 1 ==> dest[jj].intValue() > new_value.intValue());
            //@ loop_invariant dest != null && 0 <= jc + 1 && jc + 1 <= ic + 1 && ic + 1 <= dest.length && (\forall int jj; jc + 1 <= jj && jj < ic + 1 ==> dest[jj] != null) && (\forall int jj1, jj2; jc + 1 <= jj1 && jj1 < jj2 && jj2 < ic + 1 ==> dest[jj1].intValue() <= dest[jj2].intValue());
            //@ loop_invariant !(jc == ic) ==> dest != null && 0 <= low && low <= jc && jc <= dest.length && (\forall int jj; low <= jj && jj < jc ==> dest[jj] != null) && dest[jc + 1] != null && (\forall int jj; low <= jj && jj < jc ==> dest[jj].intValue() <= dest[jc + 1].intValue());
            //@ loop_invariant dest != null && 0 <= low && low <= high && high <= dest.length && (\forall int jj; low <= jj && jj < high ==> dest[jj] != null);
            //@ loop_invariant dest != null && 0 <= low && low <= ic && ic <= dest.length && (\forall int jj; low <= jj && jj < ic ==> dest[jj] != null) && (\forall int jj1, jj2; low <= jj1 && jj1 < jj2 && jj2 < ic ==> dest[jj1].intValue() <= dest[jj2].intValue());
            //@ loop_invariant dest != null && 0 <= low && low <= jc && jc <= dest.length && (\forall int jj; low <= jj && jj < jc ==> dest[jj] != null) && (\forall int jj1, jj2; low <= jj1 && jj1 < jj2 && jj2 < jc ==> dest[jj1].intValue() <= dest[jj2].intValue());
            while (jc > low && dest[jc - 1].intValue() > new_value.intValue()) {
                dest[jc] = dest[jc - 1];
                jc--;
            }
            dest[jc] = new_value;
            ic++;
        }

    }

    private static class PartitionIndexes {
        int a;
        int b;
        int c;
        int d;
    }

    /**
     * Returns <tt>true</tt> if the two specified arrays of ints are
     * <i>equal</i> to one another.  Two arrays are considered equal if both
     * arrays contain the same number of elements, and all corresponding pairs
     * of elements in the two arrays are equal.  In other words, two arrays
     * are equal if they contain the same elements in the same order.  Also,
     * two array references are considered equal if both are <tt>null</tt>.<p>
     *
     * @param a one array to be tested for equality
     * @param a2 the other array to be tested for equality
     * @return <tt>true</tt> if the two arrays are equal
     */
    // Called "equals" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ ensures \result <==> (a == a2) || 
      @                      (a != null && a2 != null && TArrays.eq(a, 0, a.length, a2, 0, a2.length)); 
      @*/
    public static/*@ pure @*/boolean equals0(int[] a, int[] a2) {
        if (a == a2)
            return true;
        if (a == null || a2 == null)
            return false;

        int length = a.length;
        if (a2.length != length)
            return false;

        int ic = 0;

        //@ loop_invariant 0 <= ic && ic <= length;
        //@ loop_invariant TArrays.eq(a, 0, ic, a2, 0, ic);
        for (; ic < length; ic++) {
            if (a[ic] != a2[ic]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns a hash code based on the contents of the specified array.
     * For any two non-null <tt>int</tt> arrays <tt>a</tt> and <tt>b</tt>
     * such that <tt>Arrays.equals(a, b)</tt>, it is also the case that
     * <tt>Arrays.hashCode(a) == Arrays.hashCode(b)</tt>.
     *
     * <p>The value returned by this method is the same value that would be
     * obtained by invoking the {@link List#hashCode() <tt>hashCode</tt>}
     * method on a {@link List} containing a sequence of {@link Integer}
     * instances representing the elements of <tt>a</tt> in the same order.
     * If <tt>a</tt> is <tt>null</tt>, this method returns 0.
     *
     * @param a the array whose hash value to compute
     * @return a content-based hash code for <tt>a</tt>
     * @since 1.5
     */
    // Called "hashCode_a" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ ensures a == null ==> \result == 0; 
      @*/
    public static/*@ pure @*/int hashCode0(int a[]) {
        if (a == null)
            return 0;

        int result = 1;
        int ic = 0;
        //@ loop_invariant 0 <= ic && ic <= a.length;
        for (; ic < a.length; ic++) {
            int element = a[ic];
            result = 31 * result + element;
        }
        return result;
    }

    // Called "hashCode_b" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ ensures a == null ==> \result == 0; 
      @*/
    public static/*@pure@*/int hashCodeInteger(java_v.lang_v.Integer a[]) {
        if (a == null)
            return 0;

        int result = 1;

        int ic = 0;
        //@ loop_invariant 0 <= ic && ic <= a.length;
        for (; ic < a.length; ic++) {
            java_v.lang_v.Integer element = a[ic];
            result = 31 * result + (element == null ? 0 : element.hashCode0());
        }
        return result;
    }

    /**
     * Assigns the specified Object reference to each element of the specified
     * array of Objects.
     *
     * @param a the array to be filled
     * @param val the value to be stored in all elements of the array
     * @throws ArrayStoreException if the specified value is not of a
     *         runtime type that can be stored in the specified array
     */
    // Called "fill_c" in http://arxiv.org/abs/1407.5286
    /*@ 
      @ requires a != null;
      @
      @ assignable a[*];
      @
      @ ensures val != null ==> TArrays.eq(a, 0, a.length, val); 
      @ ensures val != null ==> TArrays.nonnull(a, 0, a.length); 
      @ ensures val == null ==> TArrays.eqnull(a, 0, a.length); 
      @*/
    public static void fillInteger0(java_v.lang_v.Integer[] a,
                                    java_v.lang_v.Integer val) {
        int ic = 0, len = a.length;

        //@ loop_invariant 0 <= ic && ic <= a.length;
        //@ loop_invariant val != null ==> TArrays.nonnull(a, 0, ic);
        //@ loop_invariant val != null ==> TArrays.eq(a, 0, ic, val);
        //@ loop_invariant val == null ==> TArrays.eqnull(a, 0, ic);
        for (; ic < len; ic++) {
            a[ic] = val;
        }
    }

    // Called "fill_d" in http://arxiv.org/abs/1407.5286
    /*@ normal_behavior
      @ requires a != null;
      @ requires TArrays.within(a, fromIndex, toIndex);
      @ assignable a[*];
      @ ensures (\forall int i; 0 <= i && i < fromIndex ==> a[i] == \old(a[i]));
      @ ensures (\forall int i; toIndex <= i && i < a.length ==> a[i] == \old(a[i]));
      @ ensures val != null ==> TArrays.nonnull(a, fromIndex, toIndex);
      @ ensures val != null ==> TArrays.eq(a, fromIndex, toIndex, val);
      @ ensures val == null ==> TArrays.eqnull(a, fromIndex, toIndex);
      @
      @ also exceptional_behavior
      @ requires fromIndex > toIndex;
      @ signals (java_v.lang_v.IllegalArgumentException ex) true;
      @
      @ also exceptional_behavior
      @ requires fromIndex < 0 || toIndex > a.length;
      @ signals (java_v.lang_v.ArrayIndexOutOfBoundsException  ex) true;
      @
      @ also exceptional_behavior
      @ requires a == null;
      @ signals (NullPointerException ex) true;
      @*/
    public static void fillInteger1(java_v.lang_v.Integer[] a, int fromIndex,
                                    int toIndex, java_v.lang_v.Integer val)
        throws java_v.lang_v.ArrayIndexOutOfBoundsException,
               NullPointerException, java_v.lang_v.IllegalArgumentException {
        int ic = fromIndex;
        if (a == null)
            throw new NullPointerException();

        rangeCheck(a.length, fromIndex, toIndex);

        //@ loop_invariant fromIndex <= ic && ic <= toIndex;
        //@ loop_invariant (\forall int i; 0 <= i && i < fromIndex ==> a[i] == \old(a[i]));
        //@ loop_invariant (\forall int i; toIndex <= i && i < a.length ==> a[i] == \old(a[i]));
        //@ loop_invariant val != null ==> TArrays.nonnull(a, fromIndex, ic);
        //@ loop_invariant val != null ==> TArrays.eq(a, fromIndex, ic, val);
        //@ loop_invariant val == null ==> TArrays.eqnull(a, fromIndex, ic);
        for (; ic < toIndex; ic++) {
            a[ic] = val;
        }
    }

}
